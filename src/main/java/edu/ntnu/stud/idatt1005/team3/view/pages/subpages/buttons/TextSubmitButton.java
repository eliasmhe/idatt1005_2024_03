package edu.ntnu.stud.idatt1005.team3.view.pages.subpages.buttons;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class TextSubmitButton extends Button {

  private final TextField textField;

  public TextSubmitButton(TextField textField) {
    super("Submit");
    this.textField = textField;
    String text = textField.getText();
    this.setOnAction(e -> {
      System.out.println("Text submitted: " + text);
    });
  }
}

package edu.ntnu.stud.idatt1005.team3.view.pages;

import edu.ntnu.stud.idatt1005.team3.controller.SceneController;
import edu.ntnu.stud.idatt1005.team3.controller.ShoppingListController;
import edu.ntnu.stud.idatt1005.team3.database.TheCookbookDB;
import edu.ntnu.stud.idatt1005.team3.model.FoodItem;
import edu.ntnu.stud.idatt1005.team3.view.sidebar.SideBar;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * The shopping list page of the application.
 */
public class ShoppingListPage {
  private final StackPane shoppingListRoot = new StackPane();
  private List<FoodItem> shoppingListList;
  private final TheCookbookDB db;
  private final SceneController sceneController;
  private final ShoppingListController slController;
  VBox shoppingListContent = new VBox(10);

  /**
   * Creates the shopping list page of the application.
   *
   * @param sceneController The scene controller controlling the switching between scenes.
   * @param db              The database controller for the application.
   * @throws FileNotFoundException If the stylesheet file is not found.
   */
  public ShoppingListPage(SceneController sceneController, TheCookbookDB db,
                          ShoppingListController slController)
      throws FileNotFoundException {
    this.sceneController = sceneController;
    this.db = db;
    this.slController = slController;
    this.shoppingListList = slController.checkInventory(db, slController.generateShoppingList());

    Button clear = new Button("Clear shopping list");
    clear.setOnAction(e -> {
      slController.clear();
      shoppingListList.clear();
      updateView();
    });
    Button refresh = new Button("Refresh");
    clear.setPrefSize(200, 60);
    refresh.setPrefSize(200, 60);

    refresh.setOnAction(e -> updateView());

    // Set the id of the root node
    shoppingListRoot.setId("shopping-list-root");

    // Creates container for the page elements.

    BorderPane topBar = getBorderPane();
    HBox pageElements = new HBox();
    VBox rightSide = new VBox();
    ScrollPane shoppingListView = new ScrollPane();
    shoppingListView.setPrefWidth(4000);
    shoppingListView.setMinHeight(200);
    shoppingListView.setId("ingredient-view");
    shoppingListContent.setPadding(new Insets(20, 20, 20, 20));
    shoppingListView.setContent(shoppingListContent);
    // Create the sidebar and the title text
    SideBar sideBar = new SideBar(sceneController);
    sideBar.getShoppingListButton().setStyle(
        "-fx-background-color: #F9EEC3;"
            + " -fx-text-fill: #416A45; -fx-effect:"
            + " DropShadow(three-pass-box, rgba(0,0,0,0.6), 10, 0, 0, 0);");
    sideBar.getShoppingListButton().setGraphicPrimary();
    sideBar.getShoppingListButton().setOnMouseExited(e -> {
      sideBar.getShoppingListButton().setStyle(
          "-fx-background-color: #F9EEC3;"
              + " -fx-text-fill: #416A45; -fx-effect:"
              + " DropShadow(three-pass-box, rgba(0,0,0,0.6), 10, 0, 0, 0);");
    });
    Text text = new Text("Shopping List");
    text.setId("title-text");

    // Add the sidebar and the title text to the root node
    pageElements.getChildren().addAll(
        sideBar.getNode(),
        rightSide
    );

    rightSide.getChildren().addAll(
        topBar,
        shoppingListView,
        new HBox(clear, refresh)
    );

    // Add page elements to the root node
    shoppingListRoot.getChildren().addAll(
        pageElements
    );

    rightSide.setPadding(new Insets(0, 0, 20, 80));
    // Set the alignment and margin of the sidebar and the title text
    StackPane.setAlignment(sideBar.getNode(), Pos.TOP_LEFT);
    StackPane.setAlignment(rightSide, Pos.TOP_RIGHT);
    StackPane.setMargin(pageElements, new Insets(20, 80, 20, 20));
    updateView();
  }

  /**
   * Returns the root node of the shopping list page.
   *
   * @return Returns the root node of the shopping list page.
   */
  public StackPane getRoot() {
    return shoppingListRoot;
  }

  /**
   * Creates a scene for the shopping list page.
   *
   * @return The scene for the shopping list page.
   */
  public Scene createScene() {
    Scene scene = new Scene(shoppingListRoot);
    scene.getStylesheets().add(
        Objects.requireNonNull(getClass().getResource("/view/stylesheet.css")).toExternalForm()
    );
    return scene;
  }

  /**
   * Sets the shopping list elements.
   */
  private BorderPane getBorderPane()
      throws FileNotFoundException {

    BorderPane topBar = new BorderPane();
    topBar.setPadding(new Insets(10, 20, 20, 0));

    // Add the title text and the new recipe button to the top bar
    Text titleText = new Text("Shopping List");
    titleText.setId("title-text");
    topBar.setLeft(titleText);
    return topBar;
  }

  /**
   * Updates the view of the shopping list page.
   */
  public void updateView() {
    shoppingListContent.getChildren().clear();
    shoppingListList = slController.checkInventory(db, slController.generateShoppingList());
    System.out.println(shoppingListList.toString());

    for (FoodItem item : shoppingListList) {
      Text text =
          new Text(item.getQuantity() + "   \t" + item.getUnit() + "         \t" + item.getName());
      text.setId("generic-text");
      shoppingListContent.getChildren().add(text);
    }
  }
}

package edu.ntnu.stud.idatt1005.team3.view.pages.subpages.buttons;

import edu.ntnu.stud.idatt1005.team3.controller.ShoppingListController;
import edu.ntnu.stud.idatt1005.team3.database.TheCookbookDB;
import edu.ntnu.stud.idatt1005.team3.model.Recipe;
import edu.ntnu.stud.idatt1005.team3.view.pages.SingleRecipePage;
import javafx.scene.control.Button;

/**
 * A button that adds a recipe to the shopping list. Used in the {@link SingleRecipePage}.
 */
public class AddToShoppingList extends Button {
  public AddToShoppingList(ShoppingListController shoppingListController, Recipe recipe,
                           TheCookbookDB db) {
    super("Add to shopping list");
    setOnAction(e -> shoppingListController.addRecipeIngredients(db, recipe.getId()));
  }
}

package edu.ntnu.stud.idatt1005.team3.view.pages.pageelements;

import edu.ntnu.stud.idatt1005.team3.controller.SceneController;
import edu.ntnu.stud.idatt1005.team3.model.Recipe;
import java.io.FileNotFoundException;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

/**
 * A class representing a recipe card. A recipe card is a visual
 * representation of a recipe summary, including only the name,
 * categories and prep time.
 */
public class RecipeCard extends Button {

  /**
   * Constructor for the RecipeCard class.
   *
   * @param recipe          The recipe to be displayed on the card.
   * @param sceneController The scene controller for the application.
   */
  public RecipeCard(Recipe recipe, SceneController sceneController) {
    super();

    // Create the text elements for the recipe card and set their styles.

    // Add the text elements to the recipe card.
    this.setText(
        recipe.getRecipeName() + "\n\n" + "Servings: " + recipe.getServings() + " portions");
    this.setAlignment(Pos.CENTER_LEFT);
    // Set the padding, width and height of the recipe card.
    this.setPadding(new Insets(20));
    this.setPrefWidth(300);
    this.setWrapText(true);

    // Set action for clicking the recipe card.
    this.setOnAction(e -> {
      try {
        sceneController.showSingleRecipePage(recipe);
      } catch (FileNotFoundException fileNotFoundException) {
        fileNotFoundException.printStackTrace();
      }
    });

    // Set the style of the recipe card.
    this.setId("recipe-card");
  }
}

package edu.ntnu.stud.idatt1005.team3.view.sidebar.buttons;

import edu.ntnu.stud.idatt1005.team3.controller.SceneController;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Class representing a button for navigating to the home page.
 */
public class HomeButton extends Button {

  /**
   * Constructs a new home button.
   *
   * @param sceneController The scene controller responsible for managing scenes.
   */
  public HomeButton(SceneController sceneController) throws FileNotFoundException {
    super("  home");

    // Set the action of the button
    this.setOnAction(e -> sceneController.showHomePage());

    setGraphicSecondary();

    mouseExitEvents();

    // Set the style of the button
    this.setAlignment(Pos.CENTER_LEFT);
    this.setMinWidth(250);
    this.setMinHeight(60);
  }

  /**
   * Sets the graphic of the button to the graphic with the primary color.
   *
   * @throws FileNotFoundException If the file is not found.
   */
  public void setGraphicPrimary() throws FileNotFoundException {
    Image homeIcon = new Image(new FileInputStream(
        "src/main/resources/icon_selected/home_primary.png"));
    ImageView homeIconView = new ImageView(homeIcon);
    homeIconView.setFitWidth(36);
    homeIconView.setFitHeight(36);
    this.setGraphic(homeIconView);
  }

  /**
   * Sets the graphic of the button to the graphic with the secondary color.
   *
   * @throws FileNotFoundException If the file is not found.
   */
  public void setGraphicSecondary() throws FileNotFoundException {
    Image homeIcon = new Image(new FileInputStream(
        "src/main/resources/icon_unselected/home_secondary.png"));
    ImageView homeIconView = new ImageView(homeIcon);
    homeIconView.setFitWidth(36);
    homeIconView.setFitHeight(36);
    this.setGraphic(homeIconView);
  }

  /**
   * Sets the mouse exit events for the button.
   */
  public void mouseExitEvents() {
    // Make sure button is changed appropriately when hovered
    this.setOnMouseEntered(e -> {
      try {
        this.setGraphicPrimary();
      } catch (FileNotFoundException fileNotFoundException) {
        fileNotFoundException.printStackTrace();
      }
    });
    this.setOnMouseExited(e -> {
      try {
        this.setGraphicSecondary();
      } catch (FileNotFoundException fileNotFoundException) {
        fileNotFoundException.printStackTrace();
      }
    });
  }
}

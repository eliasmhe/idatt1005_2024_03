package edu.ntnu.stud.idatt1005.team3;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.stud.idatt1005.team3.model.FoodItem;
import java.time.LocalTime;
import java.util.NoSuchElementException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class FoodItemTest {
  private FoodItem foodItem1;
  private FoodItem foodItem2;

  @BeforeEach
  void setUp() {
    foodItem1 = new FoodItem("Milk", 1.0, "liter", 1);
    foodItem2 = new FoodItem("Eggs", 2.0, "antall", 2);
  }

  @Nested
  @DisplayName("Testing the get methods")
  class getTests {
    @Test
    @DisplayName("Testing the getName method")
    void testGetName() {
      assertEquals("Milk", foodItem1.getName());
    }

    @Test
    @DisplayName("Testing the getName method")
    void testGetName2() {
      assertEquals("Eggs", foodItem2.getName());
    }

    @Test
    @DisplayName("Testing the getQuantity method")
    void testGetQuantity() {
      assertEquals(1, foodItem1.getQuantity());
    }

    @Test
    @DisplayName("Testing the getQuantity method")
    void testGetQuantity2() {
      assertEquals(2, foodItem2.getQuantity());
    }

    @Test
    @DisplayName("Testing the getUnit method")
    void testGetUnit() {
      assertEquals("liter", foodItem1.getUnit());
    }

    @Test
    @DisplayName("Testing the getId method")
    void testGetId() {
      assertEquals(1, foodItem1.getId());
    }

    @Test
    @DisplayName("Testing the getId method")
    void testGetId2() {
      assertEquals(2, foodItem2.getId());
    }
  }

  @Nested
  @DisplayName("Testing the set methods")
  class setTests {
    @Test
    @DisplayName("Testing the setQuantity method")
    void testSetQuantity() {
      foodItem1.setQuantity(2.0);
      assertEquals(2, foodItem1.getQuantity());
    }

    @Test
    @DisplayName("Testing the setQuantity method")
    void testSetQuantity2() {
      foodItem2.setQuantity(1.0);
      assertEquals(1, foodItem2.getQuantity());
    }

    @Test
    @DisplayName("Testing the setQuantity method with negative value")
    void testSetQuantityNegative() {
      assertThrows(IllegalArgumentException.class, () -> foodItem1.setQuantity(-1.0));
    }

  }

}

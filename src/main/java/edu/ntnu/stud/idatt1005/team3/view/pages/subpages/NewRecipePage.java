package edu.ntnu.stud.idatt1005.team3.view.pages.subpages;

import edu.ntnu.stud.idatt1005.team3.controller.ObserverDB;
import edu.ntnu.stud.idatt1005.team3.controller.SceneController;
import edu.ntnu.stud.idatt1005.team3.database.TheCookbookDB;
import edu.ntnu.stud.idatt1005.team3.model.FoodItem;
import edu.ntnu.stud.idatt1005.team3.view.customelements.DoubleTextField;
import edu.ntnu.stud.idatt1005.team3.view.sidebar.SideBar;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class NewRecipePage implements ObserverDB {
  private final StackPane newRecipeRoot = new StackPane();
  VBox newRecipeElements = new VBox();
  HBox pageElements = new HBox();
  ArrayList<FoodItem> ingredients = new ArrayList<>();
  TextField recipeName;
  DoubleTextField servings;
  TextArea instructions;
  ScrollPane ingredientView = new ScrollPane();
  VBox ingredientContent = new VBox(10);
  HBox ingredientContainer = new HBox();
  TheCookbookDB db;
  final int recipeDBID;

  public NewRecipePage(SceneController sceneController, TheCookbookDB db)
      throws FileNotFoundException {
    newRecipeRoot.setId("recipe-root");
    this.db = db;
    db.addObserver(this);
    db.clearPlaceholder();
    db.insertPlaceholderRecipe();

    this.recipeDBID = db.getRecipeId("placeholder");

    SideBar sideBar = new SideBar(sceneController);
    sideBar.getRecipeLibraryButton().setStyle(
        "-fx-background-color: #F9EEC3; -fx-text-fill: #416A45; -fx-effect: DropShadow(three-pass-box, rgba(0,0,0,0.6), 10, 0, 0, 0);");
    sideBar.getRecipeLibraryButton().setGraphicPrimary();
    sideBar.getRecipeLibraryButton().setOnMouseExited(e -> {
      sideBar.getRecipeLibraryButton().setStyle(
          "-fx-background-color: #F9EEC3; -fx-text-fill: #416A45; -fx-effect: DropShadow(three-pass-box, rgba(0,0,0,0.6), 10, 0, 0, 0);");
    });

    Text titleText = new Text("New Recipe");
    titleText.setId("title-text");
    BorderPane topBar = getBorderPane(titleText);

    recipeName = new TextField();
    servings = new DoubleTextField();
    servings.setPromptText("Servings");
    servings.setId("new-recipe-textfield");
    recipeName.setPromptText("Recipe name");
    recipeName.setId("new-recipe-textfield");

    instructions = new TextArea();
    instructions.setPromptText("Instructions");
    instructions.setPrefColumnCount(20);
    instructions.setBackground(new Background(
        new javafx.scene.layout.BackgroundFill(javafx.scene.paint.Color.web("#416A45"),
            new CornerRadii(20), null)));
    instructions.setPrefWidth(4000);
    instructions.setPrefHeight(200);
    instructions.setMinHeight(200);
    instructions.setId("new-recipe-textfield");

    ingredientContent.setPadding(new Insets(10, 0, 10, 0));
    ingredientContent.setId("ingredient-content");


    Button confirmButton = new Button("Confirm");
    Button cancelButton = new Button("Cancel");
    confirmButton.setPrefSize(250, 60);
    cancelButton.setPrefSize(250, 60);

    cancelButton.setOnAction(e -> {
      db.removeRecipe(recipeDBID);
      sceneController.showRecipeLibraryPage();
    });

    Button newIngredientButton = new Button("new ingredient");
    Button submitRecipeButton = new Button("Submit Recipe");
    submitRecipeButton.setMinHeight(60);
    submitRecipeButton.setPrefSize(250, 60);
    newIngredientButton.setPrefSize(250, 60);
    newIngredientButton.setMinHeight(60);

    AddIngredientPopUp addIngredientPopUp = new AddIngredientPopUp(db, recipeDBID, this);

    newIngredientButton.setOnAction(e -> {
      addIngredientPopUp.show();
    });

    Text instructionsText = new Text("Instructions: ");
    Text instructionsSubtext = new Text("Separate each instruction with a new line. Do not number, automatically numbered in the database <3.");

    VBox text = new VBox(instructionsText, instructionsSubtext);
    text.setSpacing(5);

    ingredientView.setId("ingredient-view");
    ingredientView.setPadding(new Insets(0, 10, 0, 10));

    HBox buttonContainer = new HBox(20);
    buttonContainer.getChildren().addAll(newIngredientButton, submitRecipeButton, cancelButton);

    newRecipeElements.getChildren().addAll(
        topBar,
        recipeName,
        servings,
        text,
        instructions,
        ingredientView,
        buttonContainer
    );

    newRecipeElements.setPadding(new Insets(0, 120, 20, 80));

    pageElements.getChildren().addAll(
        sideBar.getNode(),
        newRecipeElements
    );

    newRecipeRoot.getChildren().addAll(
        pageElements
    );

    newRecipeElements.setSpacing(20);

    ingredientView.setContent(ingredientContent);
    ingredientView.setPrefHeight(4000);

    submitRecipeButton.setOnAction(e -> {
      String recipeName = getRecipeName();
      insertRecipe(db);
      sceneController.showRecipeLibraryPage();
    });

    StackPane.setAlignment(sideBar.getNode(), javafx.geometry.Pos.TOP_LEFT);
    StackPane.setMargin(pageElements, new Insets(20, 80, 20, 20));
    StackPane.setAlignment(sideBar.getNode(), javafx.geometry.Pos.CENTER);
  }

  public StackPane getRoot() {
    return newRecipeRoot;
  }

  private static BorderPane getBorderPane(Text titleText) {

    // Add the title text and the new recipe button to the top bar
    BorderPane topBar = new BorderPane();
    topBar.setPadding(new Insets(10, 0, 20, 0));

    // Add the title text and the new recipe button to the top bar
    topBar.setLeft(titleText);

    return topBar;
  }

  private String getRecipeName() {
    return recipeName.getText();
  }

  private String getInstructions() {
    return instructions.getText();
  }

  private int getServings() {
    if (servings.getText().isEmpty() || servings.getText().isBlank() || servings == null) {
      return 0;
    }
    return Integer.parseInt(servings.getText());
  }

  private ArrayList<String> getInstructionsList() {
    String ingredientsTextBlock = instructions.getText();
    String[] ingredientsText = ingredientsTextBlock.split("\n");
    return new ArrayList<>(Arrays.asList(ingredientsText));
  }

  private void insertRecipe(TheCookbookDB db) {
    db.updatePlaceholderRecipe(recipeDBID, getRecipeName(), getServings());

    int stepnr = 1;
    for (String instruction : getInstructionsList()) {
      db.insertStep(stepnr, instruction, recipeDBID);
      stepnr++;
    }
  }

  private void updateIngredients2() {
    ingredientContainer.setId("ingredient-container");
    ingredientContent.getChildren().clear();
    if (!ingredients.isEmpty()) {
      for (FoodItem ingredients : ingredients) {
        ingredientContainer.getChildren().clear();
        Text ingName = new Text(ingredients.getName());
        Text ingQuantity = new Text(ingredients.getQuantity() + " " + ingredients.getUnit() + " ");
        ingName.setId("generic-text");
        ingQuantity.setId("generic-text");
        ingredientContent.getChildren().add(new HBox(ingQuantity, ingName));
      }
    }
  }

  private void updateIngredients() {
    ingredientContainer.setId("ingredient-container");
    ingredientContent.getChildren().clear();
    ingredients = db.getIngredientsAsFoodItemList(recipeDBID);
    if (!ingredients.isEmpty()) {
      for (FoodItem ingredient : ingredients) {
        ingredientContainer.getChildren().clear();
        Text ingName = new Text(ingredient.getName());
        Text ingQuantity = new Text(ingredient.getQuantity() + " " + ingredient.getUnit() + " ");
        ingName.setId("generic-text");
        ingQuantity.setId("generic-text");
        ingredientContent.getChildren().add(new HBox(ingQuantity, ingName));
      }
    }
  }

  @Override
  public void update() {
    updateIngredients();
  }
}

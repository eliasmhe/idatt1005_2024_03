package edu.ntnu.stud.idatt1005.team3.view.pages;

import edu.ntnu.stud.idatt1005.team3.controller.ObserverDB;
import edu.ntnu.stud.idatt1005.team3.controller.SceneController;
import edu.ntnu.stud.idatt1005.team3.database.TheCookbookDB;
import edu.ntnu.stud.idatt1005.team3.model.FoodItem;
import edu.ntnu.stud.idatt1005.team3.model.Recipe;
import edu.ntnu.stud.idatt1005.team3.view.pages.pageelements.RecipeCard;
import edu.ntnu.stud.idatt1005.team3.view.sidebar.SideBar;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Objects;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * The recipe library page of the application. The recipe library page is where the user can view
 * all the recipes they have saved in the application.
 */
public class RecipeLibraryPage implements ObserverDB {
  private final StackPane recipeRoot = new StackPane();
  private final SceneController sceneController;
  private final VBox col1;
  private final VBox col2;
  private final VBox col3;
  Button refresh;
  TheCookbookDB db;

  /**
   * Constructs the recipe library page of the application.
   *
   * @param sceneController The scene controller controlling the switching between scenes.
   * @param db              The database controller for the application.
   * @throws FileNotFoundException If the stylesheet file is not found.
   */
  public RecipeLibraryPage(SceneController sceneController, TheCookbookDB db)
      throws FileNotFoundException {
    this.sceneController = sceneController;
    this.db = db;

    // Set the id of the root node
    recipeRoot.setId("recipe-root");

    // Create the sidebar and the title text
    HBox pageElements = new HBox();
    Text titleText = new Text("Recipes");
    titleText.setId("title-text");
    SideBar sideBar = new SideBar(sceneController);
    sideBar.getRecipeLibraryButton().setStyle(
        "-fx-background-color: #F9EEC3; -fx-text-fill:"
            + " #416A45; -fx-effect:"
            + " DropShadow(three-pass-box, rgba(0,0,0,0.6), 10, 0, 0, 0);");
    sideBar.getRecipeLibraryButton().setGraphicPrimary();
    sideBar.getRecipeLibraryButton().setOnMouseExited(e -> {
      sideBar.getRecipeLibraryButton().setStyle(
          "-fx-background-color: #F9EEC3;"
              + " -fx-text-fill: #416A45; -fx-effect:"
              + " DropShadow(three-pass-box, rgba(0,0,0,0.6), 10, 0, 0, 0);");
    });

    // The containers for the recipe cards
    VBox rightSide = new VBox();
    HBox recipeElements = new HBox();
    VBox col1 = new VBox();
    VBox col2 = new VBox();
    VBox col3 = new VBox();
    this.col1 = col1;
    this.col2 = col2;
    this.col3 = col3;

    refresh = new Button("Refresh");
    refresh.setPrefHeight(60);
    refresh.setOnAction(e -> updateColumns());

    // Adds the recipe card columns to the recipeElements container
    recipeElements.getChildren().addAll(col1, col2, col3);

    // Set the spacing and alignment of the containers
    pageElements.setSpacing(80);
    recipeElements.setSpacing(20);
    recipeElements.setPrefWidth(4000);
    recipeElements.setAlignment(Pos.CENTER);
    recipeElements.setFillHeight(true);
    col1.setSpacing(20);
    col2.setSpacing(20);
    col3.setSpacing(20);

    // TEMPORARY TEST DATA
    ArrayList<String> exInstructions = new ArrayList<>();
    exInstructions.add("Boil pasta.");
    exInstructions.add("Add sauce.");
    exInstructions.add("Stir.");
    exInstructions.add("Serve.");
    for (int i = 0; i < 40; i++) {
      exInstructions.add("Step " + i);
    }
    ArrayList<FoodItem> exIngredients = new ArrayList<>();
    exIngredients.add(new FoodItem("Pasta", 100d, "g", 1));
    exIngredients.add(new FoodItem("Sauce", 100d, "g", 1));
    exIngredients.add(new FoodItem("Salt", 1d, "pinch", 1));
    exIngredients.add(new FoodItem("Pepper", 1d, "pinch", 1));

    Recipe recipe1 = new Recipe(1, "Pasta", exInstructions, 0, null, 0, exIngredients);
    Recipe recipe2 = new Recipe(2, "Pizza", exInstructions, 0, null, 0, exIngredients);
    Recipe recipe3 = new Recipe(3, "Salad", exInstructions, 0, null, 0, exIngredients);
    Recipe recipe4 = new Recipe(4, "Soup", exInstructions, 0, null, 0, exIngredients);
    Recipe recipe5 = new Recipe(5, "Sandwich", exInstructions, 0, null, 0, exIngredients);

    this.addRecipeCard(new RecipeCard(recipe1, sceneController), col1, col2, col3);
    this.addRecipeCard(new RecipeCard(recipe2, sceneController), col1, col2, col3);
    this.addRecipeCard(new RecipeCard(recipe3, sceneController), col1, col2, col3);
    this.addRecipeCard(new RecipeCard(recipe4, sceneController), col1, col2, col3);
    this.addRecipeCard(new RecipeCard(recipe5, sceneController), col1, col2, col3);

    // Create the new recipe button
    //TODO: Move this to its own class
    Button newRecipeButton = new Button();
    newRecipeButton.setId("new-recipe-button");
    newRecipeButton.setOnAction(e -> {
      try {
        sceneController.showNewRecipePage();
      } catch (FileNotFoundException ex) {
        throw new RuntimeException(ex);
      }
    });

    BorderPane topBar = getBorderPane(sceneController, titleText);

    // Add the top bar and the recipe elements to the right side container
    rightSide.getChildren().addAll(topBar, recipeElements);

    // Add the sidebar and the right side container to the page elements container
    pageElements.getChildren().addAll(
        sideBar.getNode(),
        rightSide
    );

    // Add the page elements container to the root node
    recipeRoot.getChildren().addAll(
        pageElements
    );

    // Set the alignment and margin of the sidebar and the right side container
    StackPane.setAlignment(sideBar.getNode(), Pos.TOP_LEFT);
    StackPane.setAlignment(recipeElements, Pos.TOP_RIGHT);
    StackPane.setMargin(pageElements, new Insets(20, 80, 20, 20));
    update();
  }

  private BorderPane getBorderPane(SceneController sceneController, Text titleText)
      throws FileNotFoundException {
    Button newRecipeButton = new Button("  new recipe");
    newRecipeButton.setId("new-recipe-button");
    Image iconDefault =
        new Image(new FileInputStream("src/main/resources/icon_unselected/plus_secondary.png"));
    Image iconSelected =
        new Image(new FileInputStream("src/main/resources/icon_selected/plus_primary.png"));
    ImageView iconDefaultView = new ImageView(iconDefault);
    ImageView iconSelectedView = new ImageView(iconSelected);

    iconDefaultView.setFitHeight(25);
    iconDefaultView.setFitWidth(25);
    iconSelectedView.setFitWidth(25);
    iconSelectedView.setFitHeight(25);

    newRecipeButton.setGraphic(iconDefaultView);
    newRecipeButton.setAlignment(Pos.CENTER_LEFT);
    newRecipeButton.setOnMouseEntered(e -> newRecipeButton.setGraphic(iconSelectedView));
    newRecipeButton.setOnMouseExited(e -> newRecipeButton.setGraphic(iconDefaultView));
    newRecipeButton.setOnAction(e -> {
      try {
        sceneController.showNewRecipePage();
      } catch (FileNotFoundException ex) {
        throw new RuntimeException(ex);
      }
    });

    // Add the title text and the new recipe button to the top bar
    BorderPane topBar = new BorderPane();
    topBar.setPadding(new Insets(10, 0, 20, 0));

    // Add the title text and the new recipe button to the top bar
    topBar.setLeft(titleText);
    HBox buttonBox = new HBox(10);
    buttonBox.getChildren().addAll(refresh, newRecipeButton);
    topBar.setRight(buttonBox);

    return topBar;
  }

  /**
   * Returns the root node of the recipe library page.
   *
   * @return Returns the root node of the recipe library page.
   */
  public StackPane getRoot() {
    return recipeRoot;
  }

  /**
   * Creates a scene for the recipe library page.
   *
   * @return Returns the scene for the recipe library page.
   */
  public Scene createScene() {
    Scene scene = new Scene(recipeRoot, 1280, 720);
    scene.getStylesheets().add(
        Objects.requireNonNull(getClass().getResource("/view/stylesheet.css")).toExternalForm()
    );
    return scene;
  }

  /**
   * Returns the scene controller for the application.
   *
   * @return Returns the scene controller for the application.
   */
  public SceneController getSceneController() {
    return sceneController;
  }

  /**
   * Adds a recipe card to the recipe library page.
   *
   * @param recipeCard The recipe card to be added.
   * @param col1       The first column of recipe cards.
   * @param col2       The second column of recipe cards.
   * @param col3       The third column of recipe cards.
   */
  public void addRecipeCard(RecipeCard recipeCard, VBox col1, VBox col2, VBox col3) {
    if (col1.getChildren().size() <= col2.getChildren().size()
        && col1.getChildren().size() <= col3.getChildren().size()) {
      col1.getChildren().add(recipeCard);
    } else if (col2.getChildren().size() <= col1.getChildren().size()
        && col2.getChildren().size() <= col3.getChildren().size()) {
      col2.getChildren().add(recipeCard);
    } else {
      col3.getChildren().add(recipeCard);
    }
  }

  /**
   * Removes a recipe card from the recipe library page.
   *
   * @param recipeCard The recipe card to be removed.
   * @param col1       The first column of recipe cards.
   * @param col2       The second column of recipe cards.
   * @param col3       The third column of recipe cards.
   */
  public void removeRecipeCard(RecipeCard recipeCard, VBox col1, VBox col2, VBox col3) {
    if (col1.getChildren().contains(recipeCard)) {
      col1.getChildren().remove(recipeCard);
    } else if (col2.getChildren().contains(recipeCard)) {
      col2.getChildren().remove(recipeCard);
    } else if (col3.getChildren().contains(recipeCard)) {
      col3.getChildren().remove(recipeCard);
    } else {
      System.out.println("Recipe card not found");
      // TODO: Add display/warning text instead of sout
    }
  }

  /**
   * Clears all recipe cards from the recipe library page.
   *
   * @param col1 The first column of recipe cards.
   * @param col2 The second column of recipe cards.
   * @param col3 The third column of recipe cards.
   */
  public void clearRecipeCards(VBox col1, VBox col2, VBox col3) {
    col1.getChildren().clear();
    col2.getChildren().clear();
    col3.getChildren().clear();
  }

  /**
   * Handles the update of the recipe library page when database is updated.
   *
   * @param recipe          The recipe to be updated.
   * @param updateType      The type of update.
   * @param sceneController The scene controller for the application.
   * @param col1            The first column of recipe cards.
   * @param col2            The second column of recipe cards.
   * @param col3            The third column of recipe cards.
   */
  public void handleUpdate(Recipe recipe, int updateType, SceneController sceneController,
                           VBox col1, VBox col2, VBox col3) {
    if (updateType == 1) {
      RecipeCard recipeCard = new RecipeCard(recipe, sceneController);
      this.addRecipeCard(recipeCard, col1, col2, col3);
    } else if (updateType == 0) {
      RecipeCard recipeCard = new RecipeCard(recipe, sceneController);
      this.removeRecipeCard(recipeCard, col1, col2, col3);
    }
  }

  /**
   * Updates the columns of the recipe library page.
   */
  public void updateColumns() {
    col1.getChildren().clear();
    col2.getChildren().clear();
    col3.getChildren().clear();

    ArrayList<Recipe> recipes = db.getAllRecipes();

    for (Recipe recipe : recipes) {
      RecipeCard recipeCard = new RecipeCard(recipe, sceneController);
      this.addRecipeCard(recipeCard, col1, col2, col3);
    }
  }

  /**
   * Updates the recipe library page when the database is updated.
   */
  @Override
  public void update() {
    updateColumns();
  }
}
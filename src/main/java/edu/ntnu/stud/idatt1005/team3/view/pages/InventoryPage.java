package edu.ntnu.stud.idatt1005.team3.view.pages;

import edu.ntnu.stud.idatt1005.team3.controller.ObserverDB;
import edu.ntnu.stud.idatt1005.team3.controller.SceneController;
import edu.ntnu.stud.idatt1005.team3.database.TheCookbookDB;
import edu.ntnu.stud.idatt1005.team3.model.FoodItem;
import edu.ntnu.stud.idatt1005.team3.view.pages.subpages.NewFoodItemPopUp;
import edu.ntnu.stud.idatt1005.team3.view.sidebar.SideBar;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Objects;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * The inventory page of the application.
 */
public class InventoryPage implements ObserverDB {
  private final StackPane inventoryRoot = new StackPane();
  private final SceneController sceneController;
  private final TheCookbookDB db;
  VBox inventoryContent;


  /**
   * Creates the inventory page of the application.
   *
   * @param sceneController The scene controller controlling the switching between scenes.
   */
  public InventoryPage(SceneController sceneController, TheCookbookDB db)
      throws FileNotFoundException {
    this.sceneController = sceneController;
    this.db = db;
    this.inventoryContent = new VBox(10);
    db.addObserver(this);


    // Set the id of the root node
    inventoryRoot.setId("inventory-root");

    // Create the sidebar and the title text
    SideBar sideBar = new SideBar(sceneController);
    sideBar.getInventoryButton().setStyle(
        "-fx-background-color: #F9EEC3; -fx-text-fill: #416A45; -fx-effect: DropShadow(three-pass-box, rgba(0,0,0,0.6), 10, 0, 0, 0);");
    sideBar.getInventoryButton().setGraphicPrimary();
    sideBar.getInventoryButton().setOnMouseExited(e -> {
      sideBar.getInventoryButton().setStyle(
          "-fx-background-color: #F9EEC3; -fx-text-fill: #416A45; -fx-effect: DropShadow(three-pass-box, rgba(0,0,0,0.6), 10, 0, 0, 0);");
    });
    Text text = new Text("Inventory");
    text.setId("title-text");

    BorderPane topBar = getBorderPane(sceneController, text);
    HBox pageElements = new HBox();
    VBox rightSide = new VBox();
    ScrollPane inventoryListView = new ScrollPane();
    VBox textDisplay = new VBox();
    inventoryContent.setPadding(new Insets(20, 20, 20, 20));
    inventoryListView.setContent(inventoryContent);
    //    inventoryListView.setPrefHeight(4000);
    topBar.setMinHeight(90);
    inventoryListView.setId("inventory-list");


    rightSide.getChildren().addAll(
        topBar,
        inventoryListView
    );

    pageElements.getChildren().addAll(
        sideBar.getNode(),
        rightSide
    );

    // Add the sidebar and the title text to the root node
    inventoryRoot.getChildren().addAll(
        pageElements
    );


    // Set the alignment and margin of the sidebar and the title text
    StackPane.setAlignment(sideBar.getNode(), Pos.TOP_LEFT);
    StackPane.setMargin(pageElements, new Insets(20, 80, 20, 20));

    pageElements.setSpacing(80);
    topBar.setPrefWidth(4000);
    updateView();
  }

  /**
   * Returns the root node of the inventory page.
   *
   * @return Returns the root node of the inventory page.
   */
  public StackPane getRoot() {
    return inventoryRoot;
  }

  /**
   * Creates a scene for the inventory page.
   *
   * @return The scene for the inventory page.
   */
  public Scene createScene() {
    Scene scene = new Scene(inventoryRoot, 1280, 720);
    scene.getStylesheets().add(
        Objects.requireNonNull(getClass().getResource("/view/stylesheet.css")).toExternalForm()
    );
    return scene;
  }

  private BorderPane getBorderPane(SceneController sceneController, Text titleText)
      throws FileNotFoundException {
    Button newFoodItemButton = new Button("  new food item");
    Button refresh = new Button("Refresh");
    refresh.setPrefHeight(60);
    HBox buttonContainer = new HBox(10);
    buttonContainer.getChildren().addAll(refresh, newFoodItemButton);
    refresh.setOnAction(e -> updateView());
    newFoodItemButton.setId("new-recipe-button");
    Image iconDefault =
        new Image(new FileInputStream("src/main/resources/icon_unselected/plus_secondary.png"));
    Image iconSelected =
        new Image(new FileInputStream("src/main/resources/icon_selected/plus_primary.png"));
    ImageView iconDefaultView = new ImageView(iconDefault);
    ImageView iconSelectedView = new ImageView(iconSelected);

    iconDefaultView.setFitHeight(25);
    iconDefaultView.setFitWidth(25);
    iconSelectedView.setFitWidth(25);
    iconSelectedView.setFitHeight(25);

    newFoodItemButton.setGraphic(iconDefaultView);
    newFoodItemButton.setAlignment(Pos.CENTER_LEFT);
    newFoodItemButton.setOnMouseEntered(e -> newFoodItemButton.setGraphic(iconSelectedView));
    newFoodItemButton.setOnMouseExited(e -> newFoodItemButton.setGraphic(iconDefaultView));

    newFoodItemButton.setOnAction(e -> {
      new NewFoodItemPopUp(sceneController, db).show();
    });

    // Add the title text and the new recipe button to the top bar
    BorderPane topBar = new BorderPane();
    topBar.setPadding(new Insets(10, 0, 20, 0));

    // Add the title text and the new recipe button to the top bar
    topBar.setLeft(titleText);
    topBar.setRight(buttonContainer);
    return topBar;
  }

  /**
   * Clears the inventory content.
   */
  public void clearInventoryContent() {
    inventoryContent.getChildren().clear();
  }

  public void updateView() {
    inventoryContent.getChildren().clear();
    ArrayList<FoodItem> foodItems = db.getNonEmptyFoodItems();
    System.out.println(foodItems.size());
    for (FoodItem foodItem : foodItems) {
      Text text = new Text(
          foodItem.getQuantity()
              + "  \t\t"
              + foodItem.getUnit()
              + "  \t\t"
              + foodItem.getName());
      text.setId("generic-text");
      inventoryContent.getChildren().add(text);
    }
  }

  /**
   * Updates the inventory content.
   */
  @Override
  public void update() {
    updateView();
  }
}

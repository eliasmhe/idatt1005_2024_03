package edu.ntnu.stud.idatt1005.team3.view.pages.subpages;

import edu.ntnu.stud.idatt1005.team3.database.TheCookbookDB;
import edu.ntnu.stud.idatt1005.team3.model.FoodItem;
import edu.ntnu.stud.idatt1005.team3.view.customelements.DoubleTextField;
import java.util.ArrayList;
import java.util.Objects;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.text.Text;

public class AddIngredientPopUp extends Stage {
  TextField ingredientName;
  DoubleTextField quantity;
  ComboBox<String> unit;
  ArrayList<FoodItem> ingredients;
  TheCookbookDB db;
  VBox ingredientContent;
  int recipeDBID;

  public AddIngredientPopUp(TheCookbookDB db, int recipeDBID, NewRecipePage newRecipePage) {
    super();

    this.db = db;
    this.recipeDBID = recipeDBID;
    StackPane recipePageRoot = new StackPane();
    recipePageRoot.setId("recipe-page");
    HBox pageElements = new HBox();
    VBox ingredientWindow = new VBox();
    ScrollPane ingredientView = new ScrollPane();
    ingredientContent = new VBox();
    ingredientView.setContent(ingredientContent);
    ingredientView.setPrefHeight(504);
    ingredientView.setId("ingredient-view");
    ingredients = new ArrayList<>();
    Text titleText = new Text("Recipe Ingredients");
    ingredientWindow.getChildren().addAll(titleText, ingredientView);
    ingredientWindow.setPrefSize(732, 504);
    ingredientWindow.setPadding(new Insets(50, 28, 50, 50));

    HBox cancelConfirmButton = new HBox();

    Button confirm = new Button("Confirm");
    Button cancel = new Button("Cancel");
    cancelConfirmButton.setSpacing(10);
    confirm.setPrefSize(125.5, 60);
    cancel.setPrefSize(125.5, 60);


    cancelConfirmButton.getChildren().addAll(cancel, confirm);

    cancel.setOnAction(e -> {
      for (FoodItem foodItem : ingredients) {
        db.removeIngredient(foodItem.getName(), recipeDBID);
      }
      ingredients.clear();
      ingredientContent.getChildren().clear();
      newRecipePage.update();
      this.close();
    });
    confirm.setOnAction(e -> {
      newRecipePage.update();
      this.close();
    });

    VBox ingredientInput = new VBox();
    Text newFoodItemText = new Text("New food item");
    ingredientName = new TextField();
    Text enterInfoText = new Text("Amount (ex: 1, 2.3, 0.5)");
    quantity = new DoubleTextField();
    unit = new ComboBox<>();
    unit.getItems().addAll("g", "dl", "pk", "stk", "ss", "ts");
    Button submitIngredient = new Button("Add Ingredient");
    submitIngredient.setPrefSize(281, 60);

    ingredientContent.setPadding(new Insets(20, 20, 20, 20));

    ingredientName.setPromptText("Ingredient name");
    unit.setPromptText("Unit");

    ingredientInput.getChildren().addAll(
        newFoodItemText,
        ingredientName,
        enterInfoText,
        quantity,
        unit,
        submitIngredient,
        cancelConfirmButton
    );

    submitIngredient.setOnAction(e -> addIngredient());

    ingredientInput.setPrefSize(281, 504);
    ingredientInput.setPadding(new Insets(50, 50, 50, 28));
    ingredientInput.setSpacing(16);

    pageElements.getChildren().addAll(ingredientWindow, ingredientInput);

    recipePageRoot.getChildren().add(pageElements);

    Scene scene = new Scene(recipePageRoot, 1120, 604);
    scene.getStylesheets().add(
        Objects.requireNonNull(getClass().getResource("/view/stylesheet.css")).toExternalForm());

    this.setResizable(false);

    // Keeps the user from accessing the rest of the application while the window is open
    this.initModality(Modality.APPLICATION_MODAL);
    this.setScene(scene);
  }

  public String getFoodItemText() {
    return ingredientName.getText().toLowerCase();
  }

  public double getQuantity() {
    return Double.parseDouble(quantity.getText());
  }

  public String getUnitText() {
    return unit.getValue();
  }

  public void addIngredient() {

    if (db.getFoodItem(getFoodItemText()) == null) {
      db.insertFoodItem(getFoodItemText(), 0, getUnitText());
    }

    int foodItemId = db.getFoodItem(getFoodItemText()).getId();

//    db.insertIngredientAsFoodItem(db.getFoodItem(getFoodItemText()), foodItemId, recipeDBID);
    db.insertIngredient(foodItemId, recipeDBID, getQuantity(), getUnitText());
    ingredients.add(db.getFoodItem(getFoodItemText()));

    Text ingText = new Text(getQuantity() + " " + getUnitText() + " " + getFoodItemText());
    ingText.setId("generic-text");

    ingredientContent.getChildren().add(ingText);
    ingredientName.clear();
    quantity.clear();
    unit.getSelectionModel().clearSelection();
  }

  public void removeIngredient(FoodItem foodItem) {
    ingredients.remove(foodItem);
    ingredientContent.getChildren().removeIf(node -> node.getId().equals(foodItem.getName()));
  }

  public ArrayList<FoodItem> getIngredients() {
    return ingredients;
  }

  public void clearIngredients() {
    ingredients.clear();
    ingredientContent.getChildren().clear();
  }
}

package edu.ntnu.stud.idatt1005.team3;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.stud.idatt1005.team3.model.FoodItem;
import edu.ntnu.stud.idatt1005.team3.model.Inventory;
import java.util.NoSuchElementException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class InventoryTest {
  Inventory inventory;
  FoodItem foodItem1;

  @BeforeEach
  void setUp() {
    inventory = new Inventory();
    foodItem1 = new FoodItem("Milk", 1.0, "liter", 1);
    inventory.addFoodItem(foodItem1);
  }

  @Nested
  @DisplayName("Testing the addFoodItem method")
  class addFoodItemTests {
    @Test
    @DisplayName("Testing the addFoodItem method")
    void testAddItem() {
      assertEquals(foodItem1, inventory.getFoodItemById(1));
    }

    @Test
    @DisplayName("Testing the addFoodItem method with the same name to throw an exception")
    void testAddItemSameName() {
      FoodItem foodItem2 = new FoodItem("Milk", 1.0, "liter", 2);
      assertThrows(IllegalArgumentException.class, () -> inventory.addFoodItem(foodItem2));
    }
  }

  @Nested
  @DisplayName("Testing the addItem method")
  class addItemTests {
    @Test
    @DisplayName("Testing the addItem method")
    void testAddItem() {
    inventory.addItem("Eggs", 2.0, "antall", 2);
      assertEquals("Eggs", inventory.getFoodItemById(2).getName());
    }

    @Test
    @DisplayName("Testing the addItem method with the same name to throw an exception")
    void testAddItemSameName() {
      assertThrows(IllegalArgumentException.class, () -> inventory.addItem("Milk", 1.0, "antall", 2));
    }
  }


  @Nested
  @DisplayName("Testing the displayInventory method")
  class displayInventoryTests {
    @Test
    @DisplayName("Testing the displayInventory method")
    void testDisplayInventory() {
      String expectedOutput = "Current Inventory:\nMilk: 1.0";
      assertEquals(expectedOutput, inventory.displayInventory());
    }

    @Test
    @DisplayName("Testing the displayInventory method with an empty inventory")
    void testDisplayInventoryEmpty() {
      inventory = new Inventory();
      assertThrows(NoSuchElementException.class, () -> inventory.displayInventory());
    }
  }

  @Nested
  @DisplayName("Testing the getFoodItemById method")
  class getFoodItemByIdTests {
    @Test
    @DisplayName("Testing the getFoodItemById method")
    void testGetFoodItemById() {
      assertEquals(foodItem1, inventory.getFoodItemById(1));
    }

    @Test
    @DisplayName("Testing the getFoodItemById method with non-existing id")
    void testGetFoodItemByIdNotExisting() {
      assertThrows(NoSuchElementException.class, () -> inventory.getFoodItemById(2));
    }

  }

}

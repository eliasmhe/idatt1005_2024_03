package edu.ntnu.stud.idatt1005.team3.view.pages;

import edu.ntnu.stud.idatt1005.team3.controller.SceneController;
import edu.ntnu.stud.idatt1005.team3.view.sidebar.SideBar;
import edu.ntnu.stud.idatt1005.team3.view.sidebar.buttons.InventoryButton;
import edu.ntnu.stud.idatt1005.team3.view.sidebar.buttons.RecipeLibraryButton;
import edu.ntnu.stud.idatt1005.team3.view.sidebar.buttons.ShoppingListButton;
import java.io.FileNotFoundException;
import java.util.Objects;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

/**
 * The home page of the application. The home page is the first page the user sees when opening the
 * application. It contains buttons for navigating to the different parts of the application.
 */
public class HomePage {
  private final StackPane homeRoot = new StackPane();
  SideBar sideBar;

  /**
   * Creates the home page of the application.
   *
   * @param sceneController The scene controller controlling the switching between scenes.
   */
  public HomePage(SceneController sceneController) throws FileNotFoundException {
    homeRoot.setId("home-root");
    sideBar = new SideBar(sceneController);
    sideBar.getHomeButton().setStyle(
        "-fx-background-color: #F9EEC3;"
            + " -fx-text-fill: #416A45; -fx-effect:"
            + " DropShadow(three-pass-box, rgba(0,0,0,0.6), 10, 0, 0, 0);");
    sideBar.getHomeButton().setGraphicPrimary();
    sideBar.getHomeButton().setOnMouseExited(e -> {
      sideBar.getHomeButton().setStyle(
          "-fx-background-color: #F9EEC3;"
              + " -fx-text-fill: #416A45; -fx-effect:"
              + " DropShadow(three-pass-box, rgba(0,0,0,0.6), 10, 0, 0, 0);");
    });


    VBox homeElements = getHomeElements(sceneController);

    homeElements.setSpacing(20);
    HBox test = new HBox();
    test.setSpacing(80);
    homeElements.setPrefWidth(4000);
    homeElements.setAlignment(Pos.CENTER);
    homeElements.setFillWidth(true);

    test.getChildren().addAll(
        sideBar.getNode(),
        homeElements
    );

    homeRoot.getChildren().addAll(
        test
    );

    StackPane.setAlignment(sideBar.getNode(), Pos.TOP_LEFT);
    StackPane.setAlignment(homeElements, Pos.TOP_RIGHT);
    StackPane.setMargin(test, new Insets(20, 80, 20, 20));
  }

  /**
   * Creates a scene for the home page.
   *
   * @return The scene for the home page.
   */

  public StackPane getRoot() {
    return homeRoot;
  }

  /**
   * Creates a scene for the home page.
   *
   * @return Returns the scene for the home page.
   */
  public Scene createScene() {
    Scene scene = new Scene(homeRoot, 1280, 720);
    scene.getStylesheets().add(
        Objects.requireNonNull(getClass().getResource("/view/stylesheet.css")).toExternalForm()
    );
    return scene;
  }

  /**
   * Creates the elements of the home page.
   *
   * @param sceneController The scene controller controlling the switching between scenes.
   * @return Returns the elements of the home page.
   * @throws FileNotFoundException If the stylesheet file is not found.
   */
  public VBox getHomeElements(SceneController sceneController) throws FileNotFoundException {
    VBox homeElements = new VBox();
    GridPane buttonGrid = new GridPane();
    Text text = new Text("Welcome to The Cookbook");
    text.setFont(Font.font("", 40));
    Text subText = new Text("What would you like to do?");


    buttonGrid.setAlignment(Pos.CENTER);
    buttonGrid.setPrefWidth(4000);

    int buttonVSize = 70;
    double buttonHSize = 4000;

    ShoppingListButton shoppingListButton = new ShoppingListButton(sceneController);
    shoppingListButton.setPrefSize(buttonHSize, buttonVSize);
    shoppingListButton.setPadding(new Insets(10, 10, 10, 30));

    InventoryButton inventoryButton = new InventoryButton(sceneController);
    inventoryButton.setPrefSize(buttonHSize, buttonVSize);
    inventoryButton.setPadding(new Insets(10, 10, 10, 30));

    RecipeLibraryButton recipeLibraryButton = new RecipeLibraryButton(sceneController);
    recipeLibraryButton.setPrefSize(buttonHSize, buttonVSize);
    recipeLibraryButton.setPadding(new Insets(10, 10, 10, 30));

    Button mealPlanButton = new Button("Week Planner");
    mealPlanButton.setAlignment(Pos.CENTER);
    mealPlanButton.setPrefSize(buttonHSize, buttonVSize);

    Button surpriseMeButton = new Button("Surprise Me");
    surpriseMeButton.setAlignment(Pos.CENTER);
    surpriseMeButton.setPrefSize(buttonHSize, buttonVSize);

    Button favoritesButton = new Button("Favorites");
    favoritesButton.setTextAlignment(TextAlignment.CENTER);
    favoritesButton.setPrefSize(buttonHSize, buttonVSize);

    text.setId("title-text");
    subText.setId("home-subtext");

    buttonGrid.add(shoppingListButton, 0, 0);
    buttonGrid.add(inventoryButton, 1, 0);
    buttonGrid.add(recipeLibraryButton, 2, 0);

    buttonGrid.setHgap(20);
    buttonGrid.setVgap(10);

    homeElements.getChildren().addAll(
        text,
        subText,
        buttonGrid
    );

    homeElements.setId("home-elements");
    homeElements.setAlignment(Pos.CENTER);

    return homeElements;
  }
}

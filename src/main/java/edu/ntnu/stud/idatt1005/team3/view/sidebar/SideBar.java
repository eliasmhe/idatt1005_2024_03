package edu.ntnu.stud.idatt1005.team3.view.sidebar;

import edu.ntnu.stud.idatt1005.team3.controller.SceneController;
import edu.ntnu.stud.idatt1005.team3.view.sidebar.buttons.HomeButton;
import edu.ntnu.stud.idatt1005.team3.view.sidebar.buttons.InventoryButton;
import edu.ntnu.stud.idatt1005.team3.view.sidebar.buttons.RecipeLibraryButton;
import edu.ntnu.stud.idatt1005.team3.view.sidebar.buttons.ShoppingListButton;
import java.io.FileNotFoundException;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * Class representing the sidebar of the application.
 */
public class SideBar {
  private final VBox sideBar = new VBox(10);

  HomeButton homeButton;
  InventoryButton inventoryButton;
  RecipeLibraryButton recipeLibraryButton;
  ShoppingListButton shoppingListButton;

  /**
   * Creates the sidebar of the application.
   *
   * @param sceneController The scene controller controlling the switching between scenes.
   * @throws FileNotFoundException If the stylesheet file is not found.
   */
  public SideBar(SceneController sceneController) throws FileNotFoundException {
    // Create the buttons for the sidebar
    homeButton = new HomeButton(sceneController);
    inventoryButton = new InventoryButton(sceneController);
    recipeLibraryButton = new RecipeLibraryButton(sceneController);
    shoppingListButton = new ShoppingListButton(sceneController);

    // Add the buttons to the sidebar
    sideBar.getChildren().addAll(
        homeButton,
        inventoryButton,
        recipeLibraryButton,
        shoppingListButton
    );

    // Set the id, background and width of the sidebar
    sideBar.setId("sidebar");

    // Set the width, padding and alignment of the sidebar
    sideBar.setMaxWidth(270);
    sideBar.setMinWidth(270);
    sideBar.setPadding(new Insets(10));
    sideBar.setAlignment(Pos.TOP_CENTER);
  }

  /**
   * Returns the sidebar of the application.
   *
   * @return Returns the sidebar of the application.
   */
  public Node getNode() {
    return sideBar;
  }


  public HomeButton getHomeButton() {
    return homeButton;
  }

  public InventoryButton getInventoryButton() {
    return inventoryButton;
  }

  public RecipeLibraryButton getRecipeLibraryButton() {
    return recipeLibraryButton;
  }

  public ShoppingListButton getShoppingListButton() {
    return shoppingListButton;
  }
}


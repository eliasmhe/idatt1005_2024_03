package edu.ntnu.stud.idatt1005.team3.database;

import edu.ntnu.stud.idatt1005.team3.controller.ObserverDB;
import edu.ntnu.stud.idatt1005.team3.model.FoodItem;
import edu.ntnu.stud.idatt1005.team3.model.Recipe;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * A class that provides methods for interacting with the database.
 */
public class TheCookbookDB {
  private static final String URL = "jdbc:mysql://localhost:3306/TheCookbookDB";
  private static final String USERNAME = "user_1";
  private static final String PASSWORD = "User_123";
  ArrayList<ObserverDB> observers = new ArrayList<>();

  // Establishing a database connection
  private Connection getConnection() throws SQLException {
    return DriverManager.getConnection(URL, USERNAME, PASSWORD);
  }

  /**
   * Inserting a new FoodItem to the database.
   *
   * @param name     the name of the item
   * @param quantity the current quantity of the item
   * @param unit     the unit used to measure the item
   */
  public void insertFoodItem(String name, double quantity, String unit) {
    try (Connection conn = getConnection();
         PreparedStatement statement = conn.prepareStatement(
             "INSERT INTO FoodItems (name, quantity, unit) VALUES (?, ?, ?)")) {
      statement.setString(1, name);
      statement.setDouble(2, quantity);
      statement.setString(3, unit);
      statement.executeUpdate();
    } catch (SQLException e) {
      System.err.println("Error inserting Fooditem: " + e.getMessage());
    }
    notifyObservers();
  }

  /**
   * Inserting a new recipe to the database.
   *
   * @param name     the name of the recipe
   * @param servings the amount of serving the recipe will make
   */
  public void insertRecipe(String name, int servings) {
    try (Connection conn = getConnection();
         PreparedStatement statement = conn.prepareStatement(
             "INSERT INTO Recipes (name, servings) VALUES (?, ?)")) {
      statement.setString(1, name);
      statement.setInt(2, servings);
      statement.executeUpdate();
    } catch (SQLException e) {
      System.err.println("Error inserting recipe: " + e.getMessage());
    }
    notifyObservers();
  }

  /**
   * Inserts a new ingredient to the database, where we can connect it to a food item and a recipe.
   *
   * @param food_item_id a reference to the ingredient in the FoodItem table
   * @param recipe_id    a reference to the recipe the ingredient will be used for
   * @param quantity     the quantity used in the recipe
   * @param unit         the unit used for measurement in the recipe
   */
  public void insertIngredient(int food_item_id, int recipe_id, double quantity, String unit) {
    try (Connection conn = getConnection();
         PreparedStatement statement = conn.prepareStatement(
             "INSERT INTO Ingredients(food_item_id, recipe_id, quantity, unit)"
                 + " VALUES (?, ?, ?, ?)")) {
      statement.setInt(1, food_item_id);
      statement.setInt(2, recipe_id);
      statement.setDouble(3, quantity);
      statement.setString(4, unit);
      statement.executeUpdate();
    } catch (SQLException e) {
      System.err.println("Error inserting ingredient: " + e.getMessage());
    }
    notifyObservers();
  }

  /**
   * Inserts a new category to the database, where we can connect it to a recipe.
   *
   * @param recipe_id a reference to the recipe the category will be used for
   * @param name      the name of the category
   */
  public void insertRecipeCategory(int recipe_id, String name) {
    try (Connection conn = getConnection();
         PreparedStatement statement = conn.prepareStatement(
             "INSERT INTO RecipeCategories(recipe_id, name) VALUES (?, ?)")) {
      statement.setInt(1, recipe_id);
      statement.setString(2, name);
      statement.executeUpdate();
    } catch (SQLException e) {
      System.err.println("Error inserting category: " + e.getMessage());
    }
    notifyObservers();
  }

  /**
   * Inserting a step in the recipe.
   *
   * @param step_number the number of the step in the recipe
   * @param description what to do in the current step
   * @param recipe_id   the recipe the step is used for
   */
  public void insertStep(int step_number, String description, int recipe_id) {
    try (Connection conn = getConnection();
         PreparedStatement statement = conn.prepareStatement(
             "INSERT INTO Steps(step_number, description, recipe_id) VALUES (?, ?, ?)")) {
      statement.setInt(1, step_number);
      statement.setString(2, description);
      statement.setInt(3, recipe_id);
      statement.executeUpdate();
    } catch (SQLException e) {
      System.err.println("Error inserting step: " + e.getMessage());
    }
    notifyObservers();
  }

  /**
   * Retrieves a recipe from the database.
   *
   * @param recipeName the name of the recipe you want to retrieve
   * @return returning the chosen recipe id, name and servings,
   * and then gets the steps and ingredients with their own method using the id
   */
  public Recipe getRecipe(String recipeName) {
    Recipe recipe = null;
    try (Connection conn = getConnection();
         PreparedStatement statement = conn.prepareStatement(
             "SELECT * FROM Recipes WHERE name = ?")) {
      statement.setString(1, recipeName);
      try (ResultSet rs = statement.executeQuery()) {
        if (rs.next()) {
          int id = rs.getInt("id");
          String name = rs.getString("name");
          int servings = rs.getInt("servings");
          ArrayList<String> instructions = getStepsForRecipe(id);
          ArrayList<FoodItem> ingredients = getIngredientsForRecipe(id);

          recipe = new Recipe(id, name, instructions, servings, null, 0, ingredients);
        }
      }
    } catch (SQLException e) {
      System.err.println("Error getting recipe: " + e.getMessage());
    }
    return recipe;
  }


  /**
   * Retrieves all recipes from the database and puts them in a list.
   *
   * @return a list that contains the names of all recipes
   */
  public List<String> getAllRecipeNames() {
    List<String> recipeNames = new ArrayList<>();
    try (Connection conn = getConnection();
         PreparedStatement statement = conn.prepareStatement("SELECT name FROM Recipes");
         ResultSet rs = statement.executeQuery()) {
      while (rs.next()) {
        String name = rs.getString("name");
        recipeNames.add(name);
      }
    } catch (SQLException e) {
      System.err.println("Error retrieving recipes: " + e.getMessage());
    }
    return recipeNames;
  }

  /**
   * Retrieves all food items from the database and puts them in a list.
   *
   * @return Returns a list of all food items.
   */
  public ArrayList<Recipe> getAllRecipes() {
    ArrayList<Recipe> recipes = new ArrayList<>();
    try (Connection conn = getConnection();
         PreparedStatement statement = conn.prepareStatement("SELECT * FROM Recipes");
         ResultSet rs = statement.executeQuery()) {
      while (rs.next()) {
        int id = rs.getInt("id");
        String name = rs.getString("name");
        int servings = rs.getInt("servings");
        ArrayList<String> instructions = getStepsForRecipe(id);
        ArrayList<FoodItem> ingredients = getIngredientsForRecipe(id);
        recipes.add(new Recipe(id, name, instructions, servings, null, 0, ingredients));
      }
    } catch (SQLException e) {
      System.err.println("Error retrieving recipes: " + e.getMessage());
    }
    return recipes;
  }

  /**
   * Retrieves all steps from the database og a chosen recipe and puts them in a list.
   *
   * @param recipe_id the id of the recipe you want to view
   * @return a list of the steps for the recipe
   */
  public ArrayList<String> getStepsForRecipe(int recipe_id) {
    ArrayList<String> steps = new ArrayList<>();
    try (Connection conn = getConnection();
         PreparedStatement statement = conn.prepareStatement(
             "SELECT description FROM Steps WHERE recipe_id = ? ")) {
      statement.setInt(1, recipe_id);
      try (ResultSet rs = statement.executeQuery()) {
        while (rs.next()) {
          String description = rs.getString("description");
          steps.add(description);
        }
      } catch (SQLException e) {
        System.err.println("Error retrieving steps: " + e.getMessage());
      }
    } catch (SQLException e) {
      System.err.println("Error retrieving steps: " + e.getMessage());
    }
    return steps;
  }

  /**
   * Retrieves a food item from the database.
   *
   * @param name The name of the food item you want to retrieve
   * @return The chosen food item
   */
  public FoodItem getFoodItem(String name) {
    FoodItem foodItem = null;
    try (Connection conn = getConnection();
         PreparedStatement statement = conn.prepareStatement(
             "SELECT * FROM FoodItems WHERE name = ?")) {
      statement.setString(1, name);
      try (ResultSet rs = statement.executeQuery()) {
        if (rs.next()) {
          int id = rs.getInt("id");
          int quantity = rs.getInt("quantity");
          String unit = rs.getString("unit");
          foodItem = new FoodItem(name, quantity, unit, id);
        }
      }
    } catch (SQLException e) {
      System.err.println("Error getting food item: " + e.getMessage());
    }
    return foodItem;
  }

  /**
   * Retrieves all the ingredients from the database og a chosen recipe and puts them in a list.
   *
   * @param recipe_id The id of the recipe you want to view
   * @return Returns a list of the ingredients for the recipe
   */
  public ArrayList<FoodItem> getIngredientsForRecipe(int recipe_id) {
    ArrayList<FoodItem> ingredients = new ArrayList<>();
    try (Connection conn = getConnection();
         PreparedStatement statement = conn.prepareStatement(
             "SELECT f.name,f.id, i.quantity, i.unit FROM Ingredients i"
                 + " INNER JOIN FoodItems f ON i.food_item_id = f.id WHERE i.recipe_id = ?")) {
      statement.setInt(1, recipe_id);
      try (ResultSet rs = statement.executeQuery()) {
        while (rs.next()) {
          String name = rs.getString("name");
          int quantity = rs.getInt("quantity");
          String unit = rs.getString("unit");
          int id = rs.getInt("id");
          FoodItem foodItem = new FoodItem(name, quantity, unit, id);
          ingredients.add(foodItem);
        }
      } catch (SQLException e) {
        System.err.println("Error retrieving ingredients: " + e.getMessage());
      }
    } catch (SQLException e) {
      System.err.println("Error retrieving ingredients: " + e.getMessage());
    }
    return ingredients;
  }

  /**
   * Retrieves all the categories from the database og a chosen recipe and puts them in a list.
   *
   * @param foodItem The id of the recipe you want to view.
   * @return Returns a list of the categories for the recipe.
   */
  public double getFoodItemQuantity(FoodItem foodItem) {
    int quantity = 0;
    String name = foodItem.getName();
    try (Connection conn = getConnection();
         PreparedStatement statement = conn.prepareStatement(
             "SELECT quantity FROM FoodItems WHERE name = ?")) {
      statement.setString(1, name);
      try (ResultSet rs = statement.executeQuery()) {
        while (rs.next()) {
          quantity = rs.getInt("quantity");
        }
      } catch (SQLException e) {
        System.err.println("Error retrieving quantity: " + e.getMessage());
      }
    } catch (SQLException e) {
      System.err.println("Error retrieving quantity: " + e.getMessage());
    }
    return quantity;
  }

  /**
   * Updates the quantity of a food item in the database.
   *
   * @param foodItemId  The ID of the food item to update.
   * @param newQuantity The new quantity of the food item.
   */
  public void updateFoodItemQuantity(int foodItemId, double newQuantity) {
    try (Connection conn = getConnection();
         PreparedStatement statement = conn.prepareStatement(
             "UPDATE FoodItems SET quantity = ? WHERE id = ?")) {
      statement.setDouble(1, newQuantity);
      statement.setInt(2, foodItemId);
      statement.executeUpdate();
    } catch (SQLException e) {
      System.err.println("Error updating food item quantity: " + e.getMessage());
    }
    notifyObservers();
  }

  /**
   * Retrieves all food items from the database that have a quantity greater than 0.
   *
   * @return A list of all food items with quantity greater than 0.
   */
  public ArrayList<FoodItem> getNonEmptyFoodItems() {
    ArrayList<FoodItem> foodItems = new ArrayList<>();
    try (Connection conn = getConnection();
         PreparedStatement statement = conn.prepareStatement(
             "SELECT * FROM FoodItems WHERE quantity > 0");
         ResultSet rs = statement.executeQuery()) {
      while (rs.next()) {
        String name = rs.getString("name");
        int quantity = rs.getInt("quantity");
        String unit = rs.getString("unit");
        int id = rs.getInt("id");
        FoodItem foodItem = new FoodItem(name, quantity, unit, id);
        foodItems.add(foodItem);
      }
    } catch (SQLException e) {
      System.err.println("Error retrieving non-empty food items: " + e.getMessage());
    }
    return foodItems;
  }

  /**
   * Deletes the placeholder recipe from the database once if new recipe hasn't been made.
   */
  public void clearPlaceholder() {
    try (Connection conn = getConnection()) {
      conn.setAutoCommit(false); // Start transaction

      PreparedStatement statement1 = conn.prepareStatement(
          "DELETE i FROM Ingredients i JOIN Recipes r ON i.recipe_id = r.id WHERE r.name = ?");
      statement1.setString(1, "placeholder");
      statement1.executeUpdate();

      PreparedStatement statement2 = conn.prepareStatement(
          "DELETE s FROM Steps s JOIN Recipes r ON s.recipe_id = r.id WHERE r.name = ?");
      statement2.setString(1, "placeholder");
      statement2.executeUpdate();

      PreparedStatement statement3 = conn.prepareStatement(
          "DELETE rc FROM RecipeCategories rc JOIN Recipes r ON "
              + "rc.recipe_id = r.id WHERE r.name = ?");
      statement3.setString(1, "placeholder");
      statement3.executeUpdate();

      PreparedStatement statement4 = conn.prepareStatement("DELETE FROM Recipes WHERE name = ?");
      statement4.setString(1, "placeholder");
      statement4.executeUpdate();

      conn.commit(); // Commit transaction
    } catch (SQLException e) {
      System.err.println("Error deleting placeholder: " + e.getMessage());
    }
    notifyObservers();
  }

  /**
   * Deletes a recipe from the database.
   *
   * @param id The ID of the recipe to delete.
   */
  public void removeRecipe(int id) {
    try (Connection conn = getConnection()) {
      conn.setAutoCommit(false); // Start transaction

      String SCRIPT1 = "DELETE FROM Ingredients WHERE recipe_id = ?";
      String SCRIPT2 = "DELETE FROM Steps WHERE recipe_id = ?";
      String SCRIPT3 = "DELETE FROM RecipeCategories WHERE recipe_id = ?";
      String SCRIPT4 = "DELETE FROM Recipes WHERE id = ?";

      PreparedStatement statement1 = conn.prepareStatement(SCRIPT1);
      statement1.setInt(1, id);
      statement1.executeUpdate();

      PreparedStatement statement2 = conn.prepareStatement(SCRIPT2);
      statement2.setInt(1, id);
      statement2.executeUpdate();

      PreparedStatement statement3 = conn.prepareStatement(SCRIPT3);
      statement3.setInt(1, id);
      statement3.executeUpdate();

      PreparedStatement statement4 = conn.prepareStatement(SCRIPT4);
      statement4.setInt(1, id);
      statement4.executeUpdate();

      conn.commit(); // Commit transaction
    } catch (SQLException e) {
      System.err.println("Error deleting placeholder: " + e.getMessage());
    }
    notifyObservers();
  }

  /**
   * Deletes a food item from the database.
   *
   * @param observer The food item to delete.
   */
  public void addObserver(ObserverDB observer) {
    observers.add(observer);
  }

  /**
   * Inserts a placeholder recipe to the database.
   */
  public void insertPlaceholderRecipe() {
    try (Connection conn = getConnection();
         PreparedStatement statement = conn.prepareStatement(
             "INSERT INTO Recipes (name, servings, prep_time) VALUES ('placeholder', 0, 0)")) {
      statement.executeUpdate();
    } catch (SQLException e) {
      System.err.println("Error inserting placeholder recipe: " + e.getMessage());
    }
    notifyObservers();
  }

  /**
   * Retrieves the ID of a recipe from the database.
   *
   * @param name The name of the recipe whose ID you want to retrieve.
   */
  public int getRecipeId(String name) {
    int id = 0;
    try (Connection conn = getConnection();
         PreparedStatement statement = conn.prepareStatement(
             "SELECT id FROM Recipes WHERE name = ?")) {
      statement.setString(1, name);
      try (ResultSet rs = statement.executeQuery()) {
        if (rs.next()) {
          id = rs.getInt("id");
        }
      }
    } catch (SQLException e) {
      System.err.println("Error getting recipe id: " + e.getMessage());
    }
    return id;
  }

  /**
   * Retrieves the ID of a food item from the database.
   *
   * @param name The name of the food item whose ID you want to retrieve.
   * @return Returns the ID of the food item.
   */
  public int getFoodItemId(String name) {
    int id = 0;
    try (Connection conn = getConnection();
         PreparedStatement statement = conn.prepareStatement(
             "SELECT id FROM FoodItems WHERE name = ?")) {
      statement.setString(1, name);
      try (ResultSet rs = statement.executeQuery()) {
        if (rs.next()) {
          id = rs.getInt("id");
        }
      }
    } catch (SQLException e) {
      System.err.println("Error getting food item id: " + e.getMessage());
    }
    return id;
  }

  /**
   * Removes a food item from the database.
   *
   * @param name The name of the food item to remove.
   */
  public void removeFoodItem(String name) {
    int id = getFoodItemId(name);
    try (Connection conn = getConnection();
         PreparedStatement statement = conn.prepareStatement(
             "DELETE FoodItems, Ingredients\n"
                 + "FROM FoodItems\n"
                 + "LEFT JOIN Ingredients ON FoodItems.id = Ingredients.food_item_id\n"
                 + "WHERE FoodItems.id = ?")) {
      statement.setInt(1, id);
      statement.executeUpdate();
    } catch (SQLException e) {
      System.err.println("Error deleting food item: " + e.getMessage());
    }
    notifyObservers();
  }

  /**
   * Retrieves the ID of an ingredient from the database.
   *
   * @param name     The name of the ingredient whose ID you want to retrieve.
   * @param recipeId The ID of the recipe the ingredient is used in.
   * @return Returns the ID of the ingredient.
   */
  public int getIngredientId(String name, int recipeId) {
    int id = 0;
    try (Connection conn = getConnection();
         PreparedStatement statement = conn.prepareStatement(
             "SELECT id FROM Ingredients WHERE recipe_id = ? AND food_item_id = ?")) {
      statement.setInt(1, recipeId);
      statement.setInt(2, getFoodItemId(name));
      try (ResultSet rs = statement.executeQuery()) {
        if (rs.next()) {
          id = rs.getInt("id");
        }
      }
    } catch (SQLException e) {
      System.err.println("Error getting ingredient id: " + e.getMessage());
    }
    return id;
  }

  /**
   * Retrieves the ID of a category from the database.
   *
   * @param recipeId     The ID of the recipe the category is used in.
   * @param foodItemName The name of the category whose ID you want to retrieve.
   * @return Returns the ID of the category.
   */
  public FoodItem getIngredientAsFoodItem(int recipeId, String foodItemName) {
    FoodItem foodItem = null;
    try (Connection conn = getConnection();
         PreparedStatement statement = conn.prepareStatement(
             "SELECT f.name, f.quantity, f.unit, i.id FROM"
                 + " Ingredients i INNER JOIN FoodItems f ON i.food_item_id = f.id"
                 + " WHERE i.recipe_id = ? AND f.name = ?")) {
      statement.setInt(1, recipeId);
      statement.setString(2, foodItemName);
      try (ResultSet rs = statement.executeQuery()) {
        if (rs.next()) {
          String name = rs.getString("name");
          double quantity = rs.getDouble("quantity");
          String unit = rs.getString("unit");
          int id = rs.getInt("id");
          foodItem = new FoodItem(name, quantity, unit, id);
        }
      }
    } catch (SQLException e) {
      System.err.println("Error getting ingredient as food item: " + e.getMessage());
    }
    return foodItem;
  }

  /**
   * Retrieves the ingredients for a recipe as a list of FoodItem objects.
   *
   * @param recipeId The ID of the recipe the ingredient is used in.
   * @return Returns the list of ingredients.
   */
  public ArrayList<FoodItem> getIngredientsAsFoodItemList(int recipeId) {
    ArrayList<FoodItem> ingredients = new ArrayList<>();
    try (Connection conn = getConnection();
         PreparedStatement statement = conn.prepareStatement(
             "SELECT f.name, i.quantity, i.unit, i.id FROM Ingredients i INNER JOIN FoodItems f ON i.food_item_id = f.id WHERE i.recipe_id = ?")) {
      statement.setInt(1, recipeId);
      try (ResultSet rs = statement.executeQuery()) {
        while (rs.next()) {
          String name = rs.getString("name");
          double quantity = rs.getDouble("quantity");
          String unit = rs.getString("unit");
          int id = rs.getInt("id");
          FoodItem foodItem = new FoodItem(name, quantity, unit, id);
          ingredients.add(foodItem);
        }
      }
    } catch (SQLException e) {
      System.err.println("Error getting ingredients as food item list: " + e.getMessage());
    }
    return ingredients;
  }

  /**
   * Gets the recipe id for an ingredient.
   *
   * @param ingredientId The id of the ingredient.
   * @return Returns the recipe id.
   */
  public int getRecipeIdFromIngredientId(int ingredientId) {
    int id = 0;
    try (Connection conn = getConnection();
         PreparedStatement statement = conn.prepareStatement(
             "SELECT recipe_id FROM Ingredients WHERE id = ?")) {
      statement.setInt(1, ingredientId);
      try (ResultSet rs = statement.executeQuery()) {
        if (rs.next()) {
          id = rs.getInt("recipe_id");
        }
      }
    } catch (SQLException e) {
      System.err.println("Error getting recipe id from ingredient id: " + e.getMessage());
    }
    return id;
  }

  /**
   * Updates the quantity of a food item in the database.
   *
   * @param foodItem The food item to update.
   * @param quantity The new quantity of the food item.
   */
  public void updateFoodItem(FoodItem foodItem, double quantity) {
    int id = foodItem.getId();
    double currentQuantity = getFoodItemQuantity(foodItem);
    double newQuantity = currentQuantity + quantity;
    try (Connection conn = getConnection();
         PreparedStatement statement = conn.prepareStatement(
             "UPDATE FoodItems SET quantity = ? WHERE id = ?")) {
      statement.setDouble(1, newQuantity);
      statement.setInt(2, id);
      statement.executeUpdate();
    } catch (SQLException e) {
      System.err.println("Error updating food item: " + e.getMessage());
    }
    notifyObservers();
  }

  /**
   * Removes an ingredient from the database.
   *
   * @param name     The name of the ingredient to remove.
   * @param recipeId The ID of the recipe the ingredient is used in.
   */
  public void removeIngredient(String name, int recipeId) {
    int foodItemId = getFoodItemId(name);

    try (Connection conn = getConnection();
         PreparedStatement statement = conn.prepareStatement(
             "DELETE FROM Ingredients WHERE food_item_id = ? AND recipe_id = ?")) {
      statement.setInt(1, foodItemId);
      statement.setInt(2, recipeId);
      statement.executeUpdate();
    } catch (SQLException e) {
      System.err.println("Error deleting ingredient: " + e.getMessage());
    }
    notifyObservers();
  }

  /**
   * Updates the placeholder recipe to a proper new
   * recipe in the NewRecipePage.
   *
   * @param id       The ID of the recipe to update.
   * @param name     The name of the recipe to update.
   * @param servings The amount of servings for the recipe.
   */
  public void updatePlaceholderRecipe(int id, String name, int servings) {
    try (Connection conn = getConnection();
         PreparedStatement statement = conn.prepareStatement(
             "UPDATE Recipes SET name = ?, servings = ? WHERE id = ?")) {
      statement.setString(1, name);
      statement.setInt(2, servings);
      statement.setInt(3, id);
      statement.executeUpdate();
    } catch (SQLException e) {
      System.err.println("Error updating placeholder recipe: " + e.getMessage());
    }
    notifyObservers();
  }

  /**
   * Deletes a food item from the database.
   *
   * @param observer The observer to remove.
   */
  public void removeObserver(ObserverDB observer) {
    observers.remove(observer);
  }

  /**
   * Notifies all observers that the database has been updated.
   */
  public void notifyObservers() {
    for (ObserverDB observer : observers) {
      observer.update();
    }
  }
}
package edu.ntnu.stud.idatt1005.team3.view.sidebar.buttons;

import edu.ntnu.stud.idatt1005.team3.controller.SceneController;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Class representing a button for navigating to the inventory page.
 */
public class InventoryButton extends Button {

  /**
   * Constructs a new inventory button.
   *
   * @param sceneController The scene controller responsible for managing scenes.
   */
  public InventoryButton(SceneController sceneController) throws FileNotFoundException {
    super("  inventory");

    // Set the action of the button
    this.setOnAction(e -> sceneController.showInventoryPage());

    Image inventoryIcon = new Image(new
        FileInputStream("src/main/resources/icon_unselected/inventory_secondary.png"));
    ImageView inventoryIconView = new ImageView(inventoryIcon);
    this.setGraphic(inventoryIconView);
    Image inventoryIconSelected = new Image(new
        FileInputStream("src/main/resources/icon_selected/inventory_primary.png"));
    ImageView inventoryIconViewSelected = new ImageView(inventoryIconSelected);

    inventoryIconView.setFitWidth(36);
    inventoryIconView.setFitHeight(36);
    inventoryIconViewSelected.setFitWidth(36);
    inventoryIconViewSelected.setFitHeight(36);

    // Make sure button is changed appropriately when hovered
    this.setOnMouseEntered(e -> {
      try {
        setGraphicPrimary();
      } catch (FileNotFoundException ex) {
        throw new RuntimeException(ex);
      }
    });
    this.setOnMouseExited(e -> {
      try {
        setGraphicSecondary();
      } catch (FileNotFoundException ex) {
        throw new RuntimeException(ex);
      }
    });

    // Set the style of the button
    this.setAlignment(Pos.CENTER_LEFT);
    this.setMinWidth(250);
    this.setMinHeight(60);
    this.setPrefSize(250, 60);
  }

  /**
   * Sets the graphic of the button to the graphic with the primary color.
   *
   * @throws FileNotFoundException If the file is not found.
   */
  public void setGraphicPrimary() throws FileNotFoundException {
    Image homeIcon = new Image(new FileInputStream(
        "src/main/resources/icon_selected/inventory_primary.png"));
    ImageView homeIconView = new ImageView(homeIcon);
    homeIconView.setFitWidth(36);
    homeIconView.setFitHeight(36);
    this.setGraphic(homeIconView);
  }

  /**
   * Sets the graphic of the button to the graphic with the secondary color.
   *
   * @throws FileNotFoundException If the file is not found.
   */
  public void setGraphicSecondary() throws FileNotFoundException {
    Image homeIcon = new Image(new FileInputStream(
        "src/main/resources/icon_unselected/inventory_secondary.png"));
    ImageView homeIconView = new ImageView(homeIcon);
    homeIconView.setFitWidth(36);
    homeIconView.setFitHeight(36);
    this.setGraphic(homeIconView);
  }
}

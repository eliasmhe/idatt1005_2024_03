package edu.ntnu.stud.idatt1005.team3.view.pages;


import edu.ntnu.stud.idatt1005.team3.controller.SceneController;
import edu.ntnu.stud.idatt1005.team3.controller.ShoppingListController;
import edu.ntnu.stud.idatt1005.team3.database.TheCookbookDB;
import edu.ntnu.stud.idatt1005.team3.model.FoodItem;
import edu.ntnu.stud.idatt1005.team3.model.Recipe;
import edu.ntnu.stud.idatt1005.team3.view.pages.subpages.buttons.AddToShoppingList;
import edu.ntnu.stud.idatt1005.team3.view.sidebar.SideBar;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * The page for displaying a single recipe.
 */
public class SingleRecipePage extends StackPane {
  TheCookbookDB db;
  SceneController sceneController;
  ShoppingListController slController;

  /**
   * Creates a new single recipe page.
   *
   * @param sceneController The scene controller responsible for managing scenes.
   * @param db              The database controller for the application.
   * @param recipe          The recipe to display.
   * @param slController    The shopping list controller for the application.
   * @throws FileNotFoundException If the file is not found.
   */
  public SingleRecipePage(SceneController sceneController,
                          TheCookbookDB db,
                          Recipe recipe, ShoppingListController slController)
      throws FileNotFoundException {
    super();

    this.db = db;
    this.sceneController = sceneController;
    this.slController = slController;

    HBox pageElements = new HBox();
    SideBar sideBar = new SideBar(sceneController);
    sideBar.getRecipeLibraryButton().setStyle(
        "-fx-background-color: #F9EEC3;"
            + " -fx-text-fill: #416A45; -fx-effect:"
            + " DropShadow(three-pass-box, rgba(0,0,0,0.6), 10, 0, 0, 0);");
    sideBar.getRecipeLibraryButton().setGraphicPrimary();
    sideBar.getRecipeLibraryButton().setOnMouseExited(e -> {
      sideBar.getRecipeLibraryButton().setStyle(
          "-fx-background-color: #F9EEC3;"
              + " -fx-text-fill: #416A45; -fx-effect:"
              + " DropShadow(three-pass-box, rgba(0,0,0,0.6), 10, 0, 0, 0);");
    });

    pageElements.setSpacing(20);

    // recipeContent
    ScrollPane recipeScrollPane = new ScrollPane();

    recipeScrollPane.setPrefSize(4000, 4000);
    VBox recipeContent = new VBox();
    recipeContent.setId("recipe-content");
    VBox middleContent = new VBox();
    middleContent.setId("middle-content");
    Text titleText = new Text(recipe.getRecipeName());
    titleText.setId("title-text");
    VBox spacer = new VBox();
    spacer.setMaxHeight(20);
    spacer.setMinHeight(20);
    VBox recipeInstructions = new VBox(10);

    VBox.setMargin(recipeContent, new Insets(20, 20, 20, 20));
    VBox.setMargin(recipeInstructions, new Insets(20, 20, 20, 20));

    addRecipeInstructions(recipe, recipeInstructions);
    recipeScrollPane.setContent(recipeContent);
    HBox topBar = getTopBar(sceneController, titleText);
    topBar.setPadding(new Insets(30, 20, 20, 23));
    topBar.setAlignment(Pos.CENTER_LEFT);

    recipeScrollPane.setId("recipe-scroll-pane");

    recipeContent.getChildren().addAll(
        recipeInstructions
    );

    middleContent.getChildren().addAll(
        topBar,
        recipeScrollPane,
        spacer
    );

    // Add all content to pageElements
    pageElements.getChildren().addAll(
        sideBar.getNode(),
        middleContent,
        getLeftBar(recipe)
    );

    // Add all content to the root node
    getChildren().addAll(
        pageElements
    );


    StackPane.setAlignment(sideBar.getNode(), Pos.TOP_LEFT);
    HBox.setMargin(sideBar.getNode(), new Insets(20, 0, 20, 20));


    recipeScrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
    recipeScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
  }

  public StackPane getRoot() {
    return this;
  }

  private static HBox getTopBar(SceneController sceneController, Text titleText)
      throws FileNotFoundException {
    Button backButton = new Button();
    backButton.setId("back-button");
    Image iconDefault =
        new Image(new FileInputStream("src/main/resources/icon_selected/back_primary.png"));
    ImageView iconDefaultView = new ImageView(iconDefault);

    iconDefaultView.setFitHeight(30);
    iconDefaultView.setFitWidth(30);

    backButton.setGraphic(iconDefaultView);
    backButton.setAlignment(Pos.CENTER_LEFT);
    backButton.setOnAction(e -> sceneController.showRecipeLibraryPage());

    // Add the title text and the new recipe button to the top bar
    HBox topBar = new HBox();
    topBar.setPadding(new Insets(10, 0, 20, 0));

    topBar.getChildren().addAll(
        backButton,
        titleText
    );
    return topBar;
  }

  /**
   * Adds the recipe instructions to the recipeInstructions VBox.
   *
   * @param recipe             The recipe to get the instructions from.
   * @param recipeInstructions The VBox to add the instructions to.
   */
  public void addRecipeInstructions(Recipe recipe, VBox recipeInstructions) {
    int counter = 1;
    Text instructionsTitleText = new Text("Instructions\n");
    instructionsTitleText.setId("sub-title");
    recipeInstructions.getChildren().add(instructionsTitleText);

    for (String instruction : recipe.getRecipeInstructions()) {
      Text instructionText = new Text(counter + "\t" + instruction);
      counter++;
      instructionText.setId("generic-text");
      recipeInstructions.getChildren().add(instructionText);
    }
  }

  /**
   * Creates the left bar of the single recipe page.
   *
   * @param recipe The recipe to get the ingredients from.
   * @return The left bar of the single recipe page.
   */
  public VBox getLeftBar(Recipe recipe) {
    VBox leftBar = new VBox();
    leftBar.setId("left-bar");
    leftBar.setSpacing(20);

    leftBar.setPadding(new Insets(20, 20, 20, 0));
    VBox buttonContainer = new VBox();
    buttonContainer.setPrefHeight(400);
    buttonContainer.setMinWidth(270);
    buttonContainer.setMaxWidth(270);
    buttonContainer.setId("button-container");
    buttonContainer.setPadding(new Insets(20, 0, 0, 0));
    VBox ingredientContainer = new VBox();
    ingredientContainer.setMinWidth(270);
    ingredientContainer.setMaxWidth(270);
    ingredientContainer.setPrefHeight(800);
    ingredientContainer.setId("ingredient-container");

    Button deleteRecipeButton = new Button("Delete recipe");
    AddToShoppingList addToShoppingListButton = new AddToShoppingList(slController, recipe, db);

    deleteRecipeButton.setOnAction(e -> {
      db.removeRecipe(recipe.getId());
      sceneController.showRecipeLibraryPage();
    });

    Text ingredientsText = new Text("Ingredients\n");
    ingredientsText.setId("sub-title");

    ingredientContainer.getChildren().add(ingredientsText);
    ingredientContainer.setSpacing(10);

    ArrayList<FoodItem> ingredients = db.getIngredientsAsFoodItemList(recipe.getId());

    for (FoodItem ingredient : recipe.getIngredients()) {

      String ingredientInfo =
          ingredient.getQuantity() + "   \t " + ingredient.getUnit() + "   \t "
              + ingredient.getName();
      Text ingredientText = new Text(ingredientInfo);
      ingredientText.setId("generic-text");
      ingredientContainer.getChildren().add(ingredientText);
    }

    ingredientContainer.setPadding(new Insets(20, 20, 20, 20));

    buttonContainer.getChildren().addAll(
        deleteRecipeButton,
        addToShoppingListButton
    );

    deleteRecipeButton.setPrefSize(250, 60);
    addToShoppingListButton.setPrefSize(250, 60);
    deleteRecipeButton.setAlignment(Pos.CENTER_LEFT);
    addToShoppingListButton.setAlignment(Pos.CENTER_LEFT);

    buttonContainer.setPadding(new Insets(20, 20, 20, 20));

    leftBar.getChildren().addAll(
        buttonContainer,
        ingredientContainer
    );

    StackPane.setAlignment(leftBar, Pos.TOP_LEFT);
    return leftBar;
  }
}

package edu.ntnu.stud.idatt1005.team3;

import edu.ntnu.stud.idatt1005.team3.model.FoodItem;
import edu.ntnu.stud.idatt1005.team3.model.Inventory;
import edu.ntnu.stud.idatt1005.team3.model.ShoppingList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

public class ShoppingListTest {

  @Nested
  @DisplayName("Positive tests for the ShoppingList class")
  class PositiveTests {
    private ShoppingList shoppingList;
    private FoodItem apple;
    private FoodItem banana;
    private FoodItem test;

    @BeforeEach
    public void setUp() {
      shoppingList = new ShoppingList(new ArrayList<>());
      apple = new FoodItem("Apple", 3.0, "antall", 1);
      banana = new FoodItem("Banana", 5.0, "antall", 2);
      test = new FoodItem("Potet", 1.0, "antall", 3);
    }

    @Test
    @DisplayName("Testing the printShoppingList method")
    public void testPrintShoppingList() {
      shoppingList.addFoodItem(apple);
      shoppingList.addFoodItem(banana);
      shoppingList.printShoppingList();
    }

    @Test
    @DisplayName("Testing the addFoodItem method")
    public void testAddFoodItem() {
      shoppingList.addFoodItem(apple);
      assertNotNull(shoppingList.findFoodItemByName("Apple"));
    }

    @Test
    @DisplayName("Testing the removeFoodItem method")
    public void testRemoveFoodItem() {
      shoppingList.addFoodItem(apple);
      shoppingList.addFoodItem(banana);
      shoppingList.removeFoodItem(apple);
      assertNull(shoppingList.findFoodItemByName("Apple"));
      assertNotNull(shoppingList.findFoodItemByName("Banana"));
    }

    @Test
    @DisplayName("Testing the findFoodItemByName method")
    public void testFindFoodItemByName() {
      shoppingList.addFoodItem(apple);
      shoppingList.addFoodItem(banana);
      assertEquals(apple, shoppingList.findFoodItemByName("Apple"));
    }

    @Test
    @DisplayName("Testing the findFoodItemById method")
    public void testFindFoodItemById() {
      shoppingList.addFoodItem(apple);
      shoppingList.addFoodItem(banana);
      assertEquals(banana, shoppingList.findFoodItemById(2));
    }
  }

  @Nested
  @DisplayName("Negative tests for the ShoppingList class")
  class NegativeTests {
    private ShoppingList shoppingList;
    private FoodItem apple;
    private FoodItem banana;

    @BeforeEach
    public void setUp() {
      shoppingList = new ShoppingList(new ArrayList<>());
      apple = new FoodItem("Apple", 3.0, "antall", 1);
      banana = new FoodItem("Banana", 5.0, "antall", 2);
    }


    @Test
    public void testFindFoodItemByNameNotFound() {
      assertNull(shoppingList.findFoodItemByName("Orange"));
    }

    @Test
    public void testFindFoodItemByIdNotFound() {
      assertNull(shoppingList.findFoodItemById(3));
    }

    @Test
    public void testFindFoodItemByNameNull() {
      assertThrows(NullPointerException.class, () -> {
        shoppingList.findFoodItemByName(null);
      });
    }

    @Test
    public void testFindFoodItemByIdNegativeId() {
      assertThrows(IllegalArgumentException.class, () -> {
        shoppingList.findFoodItemById(-1);
      });
    }

    @Test
    @DisplayName("Test addFoodItem() with null")
    public void testAddFoodItemWithNull() {
      assertThrows(NullPointerException.class, () -> {
        shoppingList.addFoodItem(null);
      });
    }

    @Test
    @DisplayName("Test removeFoodItem() with null")
    public void testRemoveFoodItemWithNull() {
      assertThrows(NullPointerException.class, () -> {
        shoppingList.removeFoodItem(null);
      });
    }
  }
}

package edu.ntnu.stud.idatt1005.team3.model;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

/**
 * Represents a food item with its name and quantity.
 */
public class FoodItem {
  private final String name;
  private Double quantity;
  private final String unit;
  private final int id;
  private final ArrayList<String> units = new ArrayList<>(
      Arrays.asList("L", "kg", "ts", "ss", "dl", "ml", "cl", "g", "stk", "båt", "fedd",
          "to taste"));

  /**
   * Constructs a food item with the given name and quantity.
   *
   * @param name     The name of the food item.
   * @param quantity The quantity of the food item.
   */
  public FoodItem(String name, double quantity, String unit, int id) {
    this.name = name;
    this.quantity = quantity;
    this.unit = unit;
    this.id = id;
  }

  /**
   * Returns the name of the food item.
   *
   * @return Returns the name of the food item.
   */
  public String getName() {
    return name;
  }

  /**
   * Returns the quantity of the food item.
   *
   * @return Returns the quantity of the food item.
   */

  public Double getQuantity() {
    return quantity;
  }

  /**
   * Returns the id of the food item.
   *
   * @return Returns the id of the food item.
   */

  public int getId() {
    return id;
  }

  /**
   * Sets the quantity of the food item.
   *
   * @param quantity The quantity of the food item.
   */

  public void setQuantity(Double quantity) {
    if (quantity < 0) {
      throw new IllegalArgumentException("Quantity cannot be negative.");
    }
    this.quantity = quantity;
  }

  /**
   * Returns the unit of the food item.
   *
   * @return Returns the unit of the food item.
   */
  public String getUnit() {
    return unit;
  }

  /**
   * Returns a string representation of the food item.
   *
   * @return Returns a string representation of the food item.
   */
  @Override
  public String toString() {
    return name + ": " + quantity + " " + unit;
  }

  /**
   * Compares the food item to another object and check if they are equal.
   *
   * @param o The object to compare to.
   * @return Returns true if the objects are equal, false otherwise.
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FoodItem foodItem = (FoodItem) o;
    return quantity == foodItem.quantity
        && id == foodItem.id
        && Objects.equals(name, foodItem.name)
        && Objects.equals(unit, foodItem.unit);
  }

  /**
   * Returns the hash code of the food item.
   *
   * @return Returns the hash code of the food item.
   */
  @Override
  public int hashCode() {
    return Objects.hash(name, quantity, unit, id);
  }
}


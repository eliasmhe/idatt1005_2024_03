package edu.ntnu.stud.idatt1005.team3.launcher;


import edu.ntnu.stud.idatt1005.team3.controller.SceneController;
import edu.ntnu.stud.idatt1005.team3.controller.ShoppingListController;
import edu.ntnu.stud.idatt1005.team3.database.TheCookbookDB;
import java.io.FileNotFoundException;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * The Main class for the User Interface.
 */
public class MainUI extends Application {

  /**
   * The main method of the UI.
   *
   * @param primaryStage The primary stage of the program.
   */
  @Override
  public void start(Stage primaryStage) throws FileNotFoundException {

    TheCookbookDB db = new TheCookbookDB();
    ShoppingListController slController = new ShoppingListController(db);
    SceneController sceneController = new SceneController(primaryStage, db, slController);
    sceneController.showHomePage();

    primaryStage.setTitle("The Cookbook");
    primaryStage.setWidth(1400);
    primaryStage.setHeight(1000);
    primaryStage.show();
  }
}

package edu.ntnu.stud.idatt1005.team3.model;

import java.util.ArrayList;

/**
 * This class represents a recipe. It contains information about the recipe, such as name, instructions,
 * servings, allergies, cook time and ingredients.
 *
 * @author augustrb
 * @version 0.2
 * @since 0.1
 */
public class Recipe {
  private int id;
  private String recipeName;
  private ArrayList<String> recipeInstructions;
  private int servings;
  private ArrayList<String> allergies;
  private int cookTime;
  private ArrayList<FoodItem> ingredients;

  /**
   * Constructor for the Recipe class.
   *
   * @param id                 The id of the recipe
   * @param recipeName         The name of the recipe.
   * @param recipeInstructions The instructions for the recipe.
   * @param servings           The number of servings the recipe makes.
   * @param allergies          The allergies the recipe contains.
   * @param cookTime           The time it takes to cook the recipe.
   * @param ingredients        The ingredients in the recipe.
   */
  public Recipe(int id, String recipeName, ArrayList<String> recipeInstructions, int servings,
                ArrayList<String> allergies, int cookTime, ArrayList<FoodItem> ingredients) {
    this.id = id;
    this.recipeName = recipeName;
    this.recipeInstructions = recipeInstructions;
    this.servings = servings;
    this.allergies = allergies;
    this.cookTime = cookTime;
    this.ingredients = ingredients;
  }

  /**
   * A method to get the id of the recipe.
   *
   * @return The id of the recipe.
   */
  public int getId() {
    return id;
  }

  /**
   * A method to get the name of the recipe.
   *
   * @return The name of the recipe.
   */
  public String getRecipeName() {
    return recipeName;
  }

  /**
   * A method to get the instructions for the recipe.
   *
   * @return The instructions for the recipe.
   */
  public ArrayList<String> getRecipeInstructions() {
    return recipeInstructions;
  }

  /**
   * A method to get the number of servings the recipe makes.
   *
   * @return The number of servings the recipe makes.
   */
  public int getServings() {
    return servings;
  }

  /**
   * A method to get the allergies the recipe contains.
   *
   * @return The allergies the recipe contains.
   */
  public ArrayList<String> getAllergies() {
    return allergies;
  }

  /**
   * A method to get the time it takes to cook the recipe.
   *
   * @return The time it takes to cook the recipe.
   */
  public int getCookTime() {
    return cookTime;
  }

  /**
   * A method to get the ingredients in the recipe.
   *
   * @return The ingredients in the recipe.
   */
  public ArrayList<FoodItem> getIngredients() {
    return ingredients;
  }

  /**
   * A method to set the name of the recipe.
   *
   * @param recipeName The name of the recipe.
   */
  public void setRecipeName(String recipeName) {
    if (recipeName == null || recipeName.isBlank()) {
      throw new IllegalArgumentException("Recipe name cannot be null or blank");
    }
    this.recipeName = recipeName;
  }

  /**
   * A method to set the id of the recipe.
   *
   * @param id The name of the recipe.
   */
  public void setId(int id) {
    if (id <= 0) {
      throw new IllegalArgumentException("Recipe id cannot be negative or 0");
    }
    this.id = id;
  }

  /**
   * A method to set the instructions for the recipe.
   *
   * @param recipeInstructions The instructions for the recipe.
   */
  public void setRecipeInstructions(ArrayList<String> recipeInstructions) {
    if (recipeInstructions == null || recipeInstructions.isEmpty()) {
      throw new IllegalArgumentException("Recipe instructions cannot be null or empty");
    }
    this.recipeInstructions = recipeInstructions;
  }

  /**
   * A method to set the number of servings the recipe makes.
   *
   * @param servings The number of servings the recipe makes.
   */
  public void setServings(int servings) {
    if (servings <= 0) {
      throw new IllegalArgumentException("Servings must be greater than 0");
    }
    this.servings = servings;
  }

  /**
   * A method to set the allergies the recipe contains.
   *
   * @param allergies The allergies the recipe contains.
   */
  public void setAllergies(ArrayList<String> allergies) {
    this.allergies = allergies;
  }

  /**
   * A method to set the time it takes to cook the recipe.
   *
   * @param cookTime The time it takes to cook the recipe.
   */
  public void setCookTime(int cookTime) {
    if (cookTime < 0) {
      throw new IllegalArgumentException("Cook time cannot be negative");
    }
    this.cookTime = cookTime;
  }

  /**
   * A method to set the ingredients in the recipe.
   *
   * @param ingredients The ingredients in the recipe.
   */
  public void setIngredients(ArrayList<FoodItem> ingredients) {
    if (ingredients == null || ingredients.isEmpty()) {
      throw new IllegalArgumentException("Ingredients cannot be null or empty");
    }
    this.ingredients = ingredients;
  }

  @Override
  public String toString() {
    return "Recipe{"
        + "id=" + id
        + ", recipeName='" + recipeName + '\''
        + ", recipeInstructions=" + recipeInstructions.toString()
        + ", servings=" + servings
        + ", allergies=" + allergies.toString()
        + ", cookTime=" + cookTime
        + ", ingredients=" + ingredients.toString()
        + '}';
  }
}

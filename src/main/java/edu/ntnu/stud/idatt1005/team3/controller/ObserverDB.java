package edu.ntnu.stud.idatt1005.team3.controller;

/**
 * An interface for TheCookbook's database observer.
 */
public interface ObserverDB {

  /**
   * Method to update the observer.
   */
  void update();
}
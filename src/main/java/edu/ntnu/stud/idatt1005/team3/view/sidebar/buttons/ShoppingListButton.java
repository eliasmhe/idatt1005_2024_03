package edu.ntnu.stud.idatt1005.team3.view.sidebar.buttons;

import edu.ntnu.stud.idatt1005.team3.controller.SceneController;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Class representing a button for the shopping list page.
 */
public class ShoppingListButton extends Button {

  /**
   * Constructs a new shopping list button.
   *
   * @param sceneController The scene controller responsible for managing scenes.
   */
  public ShoppingListButton(SceneController sceneController) throws FileNotFoundException {
    super("  shopping list");

    // Set the action of the button
    this.setOnAction(e -> sceneController.showShoppingListPage());

    this.setGraphicSecondary();

    // Make sure button is changed appropriately when hovered
    this.setOnMouseEntered(e -> {
      try {
        setGraphicPrimary();
      } catch (FileNotFoundException ex) {
        throw new RuntimeException(ex);
      }
    });
    this.setOnMouseExited(e -> {
      try {
        setGraphicSecondary();
      } catch (FileNotFoundException ex) {
        throw new RuntimeException(ex);
      }
    });

    // Set the style of the button
    this.setAlignment(Pos.CENTER_LEFT);
    this.setMinWidth(250);
    this.setMinHeight(60);
    this.setPrefSize(250, 60);
  }

  /**
   * Sets the graphic of the button to the graphic with the primary color.
   *
   * @throws FileNotFoundException If the file is not found.
   */
  public void setGraphicPrimary() throws FileNotFoundException {
    Image recipeLibraryIcon = new Image(new FileInputStream(
        "src/main/resources/icon_selected/shopping_cart_primary.png"));
    ImageView recipeLibraryIconView = new ImageView(recipeLibraryIcon);
    recipeLibraryIconView.setFitWidth(36);
    recipeLibraryIconView.setFitHeight(36);
    this.setGraphic(recipeLibraryIconView);
  }

  /**
   * Sets the graphic of the button to the graphic with the secondary color.
   *
   * @throws FileNotFoundException If the file is not found.
   */
  public void setGraphicSecondary() throws FileNotFoundException {
    Image recipeLibraryIcon = new Image(new FileInputStream(
        "src/main/resources/icon_unselected/shopping_cart_secondary.png"));
    ImageView recipeLibraryIconView = new ImageView(recipeLibraryIcon);
    recipeLibraryIconView.setFitWidth(36);
    recipeLibraryIconView.setFitHeight(36);
    this.setGraphic(recipeLibraryIconView);
  }
}

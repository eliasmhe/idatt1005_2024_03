package edu.ntnu.stud.idatt1005.team3;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.stud.idatt1005.team3.model.FoodItem;
import edu.ntnu.stud.idatt1005.team3.model.Recipe;
import edu.ntnu.stud.idatt1005.team3.model.RecipeRegister;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.NoSuchElementException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class RecipeRegisterTest {
  private RecipeRegister register;
  private Recipe recipe1;
  private Recipe recipe2;
  private Recipe recipe3;

  @BeforeEach
  public void setUp() {
    register = new RecipeRegister();

    FoodItem foodItem1 = new FoodItem("Flour", 1.0, "kg", 1);
    FoodItem foodItem2 = new FoodItem("Milk", 1.0, "liter", 2);
    FoodItem foodItem3 = new FoodItem("Eggs", 1.0, "antall", 3);

    recipe1 = new Recipe(1, "Pancakes",
        new ArrayList<>(Arrays.asList("Mix ingredients", "Fry in pan")),
        4,
        new ArrayList<>(Arrays.asList("Gluten")),
        40,
        new ArrayList<>(Arrays.asList(foodItem1, foodItem2, foodItem3))
    );

    recipe2 = new Recipe(2, "Waffles",
        new ArrayList<>(Arrays.asList("Mix ingredients", "Bake in waffle iron")),
        4,
        new ArrayList<>(Arrays.asList("Gluten")),
        30,
        new ArrayList<>(Arrays.asList(foodItem1, foodItem2, foodItem3))
    );

    recipe3 = new Recipe(3, "Pancakes",
        new ArrayList<>(Arrays.asList("Mix ingredients", "Fry in pan")),
        4,
        new ArrayList<>(Arrays.asList("Gluten")),
        20,
        new ArrayList<>(Arrays.asList(foodItem1, foodItem2, foodItem3))
    );
  }

  @Nested
  @DisplayName("Testing the addRecipe method")
  class AddRecipeTests {
    @Test
    @DisplayName("Testing the addRecipe method")
    public void testAddRecipe() {
      register.addRecipe(recipe1);
      assertTrue(register.getRecipes().contains(recipe1));
    }

    @Test
    @DisplayName("Testing the addRecipe method with null recipe")
    public void testAddNullRecipe() {
      assertThrows(NullPointerException.class, () -> register.addRecipe(null));
    }

    @Test
    @DisplayName("Testing the addRecipe method with duplicate recipe")
    public void testAddDuplicateRecipe() {
      register.addRecipe(recipe1);
      assertThrows(IllegalArgumentException.class, () -> register.addRecipe(recipe1));
    }
  }

  @Nested
  @DisplayName("Testing the removeRecipe method")
  class RemoveRecipeTests {
    @Test
    @DisplayName("Testing the removeRecipe method")
    public void testRemoveRecipe() {
      register.addRecipe(recipe1);
      register.removeRecipe(recipe1);
      assertTrue(register.getRecipes().isEmpty());
    }

    @Test
    @DisplayName("Testing the removeRecipe method with null recipe")
    public void testRemoveNullRecipe() {
      assertThrows(NullPointerException.class, () -> register.removeRecipe(null));
    }

    @Test
    @DisplayName("Testing the removeRecipe method with non-existing recipe")
    public void testRemoveNonExistingRecipe() {
      assertThrows(NoSuchElementException.class, () -> register.removeRecipe(recipe1));
    }
  }

  @Nested
  @DisplayName("Testing the removeRecipeByName method")
  class RemoveRecipeByNameTests {
    @Test
    @DisplayName("Testing the removeRecipeByName method")
    public void testRemoveRecipeByName() {
      register.addRecipe(recipe1);
      assertThrows(NoSuchElementException.class, () -> register.removeRecipeByName("Pancakes"));
    }

    @Test
    @DisplayName("Testing the removeRecipeByName method with null recipe name")
    public void testRemoveNullRecipeName() {
      assertThrows(IllegalArgumentException.class, () -> register.removeRecipeByName(null));
    }

    @Test
    @DisplayName("Testing the removeRecipeByName method with non-existing recipe name")
    public void testRemoveNonExistingRecipeName() {
      assertThrows(IllegalArgumentException.class, () -> register.removeRecipeByName("Waffles"));
    }
  }

  @Nested
  @DisplayName("Testing the getRecipes method")
  class GetRecipesTests {
    @Test
    @DisplayName("Testing the getRecipes method")
    public void testGetRecipes() {
      register.addRecipe(recipe1);
      assertTrue(register.getRecipes().contains(recipe1));
    }
  }

  @Nested
  @DisplayName("Testing the findRecipeByIngredient method")
  class FindRecipeTests {
    @Test
    @DisplayName("Testing the findRecipeByIngredient method")
    public void testFindRecipeByIngredient() {
      register.addRecipe(recipe1);
      assertTrue(register.findRecipesByIngredient("Flour").contains(recipe1));
    }

    @Test
    @DisplayName("Testing the findRecipeByIngredient method with null ingredient")
    public void testFindRecipeByNullIngredient() {
      assertThrows(IllegalArgumentException.class, () -> register.findRecipesByIngredient(null));
    }

    @Test
    @DisplayName("Testing the findRecipeByIngredient method with non-existing ingredient")
    public void testFindRecipeByNonExistingIngredient() {
      assertThrows(IllegalArgumentException.class, () -> register.findRecipesByIngredient("Sugar"));
    }

    @Test
    @DisplayName("Testing the findRecipeByIngredient method with no recipes in register")
    public void testFindRecipeByIngredientNoRecipes() {
      assertThrows(IllegalArgumentException.class, () -> register.findRecipesByIngredient("Flour"));
    }

    @Nested
    @DisplayName("Testing the findRecipeByName method")
    class FindRecipeByNameTests {
      @Test
      @DisplayName("Testing the findRecipeByName method")
      public void testFindRecipeByName() {
        register.addRecipe(recipe1);
        assertEquals(recipe1, register.findRecipeByName("Pancakes"));
      }

      @Test
      @DisplayName("Testing the findRecipeByName method with null recipe name")
      public void testFindRecipeByNullName() {
        assertThrows(IllegalArgumentException.class, () -> register.findRecipeByName(null));
      }

      @Test
      @DisplayName("Testing the findRecipeByName method with non-existing recipe name")
      public void testFindRecipeByNonExistingName() {
        assertThrows(IllegalArgumentException.class, () -> register.findRecipeByName("Waffles"));
      }

      @Test
      @DisplayName("Testing the findRecipeByName method with no recipes in register")
      public void testFindRecipeByNameNoRecipes() {
        assertThrows(IllegalArgumentException.class, () -> register.findRecipeByName("Pancakes"));
      }

    }

    @Nested
    @DisplayName("Testing the findRecipeByMaxCookTime method")
    class FindRecipeByMaxCookTimeTests {
      @Test
      @DisplayName("Testing the findRecipeByMaxCookTime method")
      public void testFindRecipeByMaxCookTime() {
        register.addRecipe(recipe1);
        assertTrue(register.findRecipesByMaxCookTime(60).contains(recipe1));
      }

      @Test
      @DisplayName("Testing the findRecipeByMaxCookTime method with negative max cook time")
      public void testFindRecipeByNegativeMaxCookTime() {
        assertThrows(IllegalArgumentException.class, () -> register.findRecipesByMaxCookTime(-1));
      }

      @Test
      @DisplayName("Testing the findRecipeByMaxCookTime method with no recipes in register")
      public void testFindRecipeByMaxCookTimeNoRecipes() {
        assertThrows(IllegalArgumentException.class, () -> register.findRecipesByMaxCookTime(60));
      }

      @Test
      @DisplayName("Testing the findRecipeByMaxCookTime method with no recipes under max cook time")
      public void testFindRecipeByMaxCookTimeNoRecipesUnderMax() {
        register.addRecipe(recipe2);
        register.addRecipe(recipe3);
        assertThrows(IllegalArgumentException.class, () -> register.findRecipesByMaxCookTime(10));
      }

      @Test
      @DisplayName("Testing the findRecipeByMaxCookTime method with max cook time of 0")
      public void testFindRecipeByMaxCookTimeZero() {
        register.addRecipe(recipe1);
        assertThrows(IllegalArgumentException.class, () -> register.findRecipesByMaxCookTime(0));
      }
    }
  }
}

package edu.ntnu.stud.idatt1005.team3.model;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * Represents an inventory of food items.
 */
public class Inventory {
  Map<String, FoodItem> items;

  /**
   * Constructs an empty inventory.
   */
  public Inventory() {
    this.items = new HashMap<>();
  }

  /**
   * Adds a food item to the inventory.
   *
   * @param name     The name of the food item.
   * @param quantity The quantity of the food item.
   */
  public void addItem(String name, Double quantity, String unit, int id) {
    if (items.containsKey(name)) {
      throw new IllegalArgumentException("The item already exists in the inventory.");
    }
    items.put(name, new FoodItem(name, quantity, unit, id));
  }

  /**
   * Adds a food item to the inventory.
   *
   * @param foodItem The food item to add.
   */
  public void addFoodItem(FoodItem foodItem) {
    if (items.containsKey(foodItem.getName())) {
      throw new IllegalArgumentException("The item already exists in the inventory.");
    }
    items.put(foodItem.getName(), foodItem);
  }

  /**
   * Displays the current inventory.
   */
  public String displayInventory() {
    if (items.isEmpty()) {
      throw new NoSuchElementException("The inventory is empty.");
    }
    StringBuilder sb = new StringBuilder("Current Inventory:\n");
    for (FoodItem foodItem : items.values()) {
      sb.append(foodItem.getName() + ": " + foodItem.getQuantity());
    }
    return sb.toString();
  }

  /**
   * Get FoodItem by ID.
   *
   * @param id The id of the food item.
   */
  public FoodItem getFoodItemById(int id) {
    for (FoodItem foodItem : items.values()) {
      if (foodItem.getId() == id) {
        return foodItem;
      }
    }
    throw new NoSuchElementException("No item with the given id exists in the inventory.");
  }
}

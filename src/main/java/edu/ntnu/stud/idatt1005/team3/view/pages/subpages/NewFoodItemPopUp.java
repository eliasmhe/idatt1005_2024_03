package edu.ntnu.stud.idatt1005.team3.view.pages.subpages;

import edu.ntnu.stud.idatt1005.team3.controller.SceneController;
import edu.ntnu.stud.idatt1005.team3.database.TheCookbookDB;
import edu.ntnu.stud.idatt1005.team3.model.FoodItem;
import edu.ntnu.stud.idatt1005.team3.view.customelements.DoubleTextField;
import java.util.ArrayList;
import java.util.Objects;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class NewFoodItemPopUp extends Stage {
  TextField nameField;
  DoubleTextField quantityField;
  ComboBox<String> unitField;
  TheCookbookDB db;
  SceneController sceneController;
  VBox content;
  ArrayList<FoodItem> tempFoodItems = new ArrayList<>();

  public NewFoodItemPopUp(SceneController sceneController, TheCookbookDB db) {
    super();
    this.db = db;
    this.sceneController = sceneController;

    VBox inputElements = new VBox();
    inputElements.setPrefSize(281, 504);
    nameField = new TextField();
    quantityField = new DoubleTextField();
    unitField = new ComboBox<>();
    unitField.getItems().addAll("g", "dl", "pk", "stk", "ss", "ts");

    Button submitFoodItemButton = new Button("Submit food item");
    submitFoodItemButton.setOnAction(e -> {
      addFoodItemToView();
    });

    HBox buttonContainer = new HBox();
    Button cancelButton = new Button("Cancel");
    cancelButton.setOnAction(e -> {
      tempFoodItems.clear();
      this.close();
    });

    Button confirmButton = new Button("Add items");
    confirmButton.setOnAction(e -> {
      submitFoodItems();
      this.close();
    });

    buttonContainer.getChildren().addAll(cancelButton, confirmButton);
    inputElements.getChildren().addAll(
        nameField,
        quantityField,
        unitField,
        submitFoodItemButton,
        buttonContainer);

    VBox viewElements = new VBox();
    Text titleText = new Text("Food items to add:");
    ScrollPane view = new ScrollPane();
    view.setPrefHeight(504);
    content = new VBox(10);
    content.setPadding(new Insets(20));
    view.setContent(content);
    viewElements.setPrefSize(732, 504);
    viewElements.setPadding(new Insets(50, 28, 50, 50));
    viewElements.getChildren().addAll(titleText, view);

    HBox pageElements = new HBox();
    pageElements.getChildren().addAll(viewElements, inputElements);
    submitFoodItemButton.setPrefSize(381, 60);
    cancelButton.setPrefSize(230.5, 60);
    confirmButton.setPrefSize(230.5, 60);
    inputElements.setPadding(new Insets(20));
    inputElements.setSpacing(20);
    unitField.setPrefSize(2381, 60);
    inputElements.setAlignment(javafx.geometry.Pos.CENTER);
    nameField.setPromptText("Name");
    quantityField.setPromptText("Quantity");
    unitField.setPromptText("Unit");
    content.setId("ingredient-view");
    view.setId("ingredient-view");


    StackPane root = new StackPane();
    root.setId("new-food-item-page");

    root.getChildren().addAll(pageElements);

    Scene scene = new Scene(root, 1120, 604);
    scene.getStylesheets().add(
        Objects.requireNonNull(getClass().getResource("/view/stylesheet.css")).toExternalForm());

    this.setResizable(false);

    this.initModality(Modality.APPLICATION_MODAL);
    this.setScene(scene);
  }

  public void showPopUp() {
    this.show();
  }

  public String getFoodItemName() {
    return nameField.getText().toLowerCase();
  }

  public double getQuantityField() {
    return Double.parseDouble(quantityField.getText());
  }

  public String getUnit() {
    return unitField.getValue();
  }

  public void addFoodItemToView() {
    Text foodItemText = new Text(getQuantityField() + "     \t" + getUnit() + "     \t" + getFoodItemName());
    foodItemText.setId("generic-text");
    tempFoodItems.add(new FoodItem(getFoodItemName(), getQuantityField(), getUnit(), 0));
    content.getChildren().add(foodItemText);
  }

  public void submitFoodItems() {
    for (FoodItem foodItem : tempFoodItems) {
      if (db.getFoodItem(foodItem.getName()) == null) {
        db.insertFoodItem(foodItem.getName(), foodItem.getQuantity(), foodItem.getUnit());
      } else {
        db.updateFoodItemQuantity(db.getFoodItem(foodItem.getName()).getId(), db.getFoodItem(foodItem.getName()).getQuantity() + foodItem.getQuantity());
      }
    }
  }
}
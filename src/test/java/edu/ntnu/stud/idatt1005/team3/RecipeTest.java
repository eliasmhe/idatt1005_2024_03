package edu.ntnu.stud.idatt1005.team3;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.stud.idatt1005.team3.model.FoodItem;
import edu.ntnu.stud.idatt1005.team3.model.Recipe;
import edu.ntnu.stud.idatt1005.team3.model.RecipeRegister;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class RecipeTest {
  RecipeRegister recipeRegister;
  Recipe recipe1;

  ArrayList<FoodItem> foodItems;

  @BeforeEach
  void setUp() {
    recipeRegister = new RecipeRegister();

    FoodItem foodItem1 = new FoodItem("Flour", 1d, "kg", 1);
    FoodItem foodItem2 = new FoodItem("Milk", 1d, "liter", 2);
    FoodItem foodItem3 = new FoodItem("Eggs", 1d, "stk", 3);

    foodItems = new ArrayList<>(Arrays.asList(
            new FoodItem("Flour", 1d, "kg", 1),
            new FoodItem("Milk", 1d, "liter", 2),
            new FoodItem("Eggs", 1d, "stk", 3)
    ));

    recipe1 = new Recipe(1, "Pancakes",
            new ArrayList<>(Arrays.asList("Mix ingredients", "Fry in pan")),
            4,
            new ArrayList<>(Arrays.asList("Gluten")),
            40,
            foodItems
    );
  }

  @Nested
  @DisplayName("Testing the get methods")
  class getTests{

    @Test
    @DisplayName("Testing the getId method")
    void testGetId() {
      assertEquals(1, recipe1.getId());
    }

    @Test
    @DisplayName("Testing the getRecipeName method")
    void testGetRecipeName() {
      assertEquals("Pancakes", recipe1.getRecipeName());
    }

    @Test
    @DisplayName("Testing the getRecipeInstructions method")
    void testGetRecipeInstructions() {
      assertEquals(recipe1.getRecipeInstructions(),
              new ArrayList<>(Arrays.asList("Mix ingredients", "Fry in pan")));
    }

    @Test
    @DisplayName("Testing the getServings method")
    void testGetServings() {
      assertEquals(4, recipe1.getServings());
    }

    @Test
    @DisplayName("Testing the getAllergies method")
    void testGetAllergies() {
      assertEquals(new ArrayList<>(Arrays.asList("Gluten")), recipe1.getAllergies());
    }

    @Test
    @DisplayName("Testing the getCookTime method")
    void testGetCookTime() {
      assertEquals(recipe1.getCookTime(), 40);
    }

    @Test
    @DisplayName("Testing the getIngredients method")
    void testGetIngredients() {
      assertEquals(recipe1.getIngredients(), foodItems);
    }
  }

  @Nested
  @DisplayName("Tests for set methods")
  class setTests{
    @Test
    @DisplayName("Testing the setId method")
    void testSetId() {
      recipe1.setId(2);
      assertEquals(2, recipe1.getId());
    }

    @Test
    @DisplayName("Testing the setId method with negative value")
    void testSetIdNegative() {
      assertThrows(IllegalArgumentException.class, () -> recipe1.setId(-1));
    }

    @Test
    @DisplayName("Testing the setRecipeName method")
    void testSetRecipeName() {
      recipe1.setRecipeName("Waffles");
      assertEquals("Waffles", recipe1.getRecipeName());
    }

    @Test
    @DisplayName("Testing the setRecipeName method with empty string")
    void testSetRecipeNameEmpty() {
      assertThrows(IllegalArgumentException.class, () -> recipe1.setRecipeName(""));
    }

    @Test
    @DisplayName("Testing the setRecipeName method with null value")
    void testSetRecipeNameNull() {
      assertThrows(IllegalArgumentException.class, () -> recipe1.setRecipeName(null));
    }

    @Test
    @DisplayName("Testing the setRecipeInstructions method")
    void testSetRecipeInstructions() {
      recipe1.setRecipeInstructions(
              new ArrayList<>(Arrays.asList("Mix ingredients", "Fry in waffle iron")));
      assertEquals(recipe1.getRecipeInstructions(),
              new ArrayList<>(Arrays.asList("Mix ingredients", "Fry in waffle iron")));
    }

    @Test
    @DisplayName("Testing the setRecipeInstructions method with empty list")
    void testSetRecipeInstructionsEmpty() {
      assertThrows(IllegalArgumentException.class,
              () -> recipe1.setRecipeInstructions(new ArrayList<>()));
    }

    @Test
    @DisplayName("Testing the setRecipeInstructions method with null value")
    void testSetRecipeInstructionsNull() {
      assertThrows(IllegalArgumentException.class, () -> recipe1.setRecipeInstructions(null));
    }

    @Test
    @DisplayName("Testing the setServings method")
    void testSetServings() {
      recipe1.setServings(6);
      assertEquals(6, recipe1.getServings());
    }

    @Test
    @DisplayName("Testing the setServings method with zero value")
    void testSetServingsZero() {
      assertThrows(IllegalArgumentException.class, () -> recipe1.setServings(0));
    }

    @Test
    @DisplayName("Testing the setServings method with negative value")
    void testSetServingsNegative() {
      assertThrows(IllegalArgumentException.class, () -> recipe1.setServings(-1));
    }

    @Test
    @DisplayName("Testing the setAllergies method")
    void testSetAllergies() {
      recipe1.setAllergies(new ArrayList<>(Arrays.asList("Lactose")));
      assertEquals(new ArrayList<>(Arrays.asList("Lactose")), recipe1.getAllergies());
    }

/*
    // Allergies should be able to set to zero, as it is not a required field
    @Test
    @DisplayName("Testing the setAllergies method with empty list")
    void testSetAllergiesEmpty() {
      assertThrows(IllegalArgumentException.class, () -> recipe1.setAllergies(new ArrayList<>()));
    }
    */

    @Test
    @DisplayName("Testing the setAllergies method with empty list")
    void testSetAllergiesEmpty() {
      assertDoesNotThrow(() -> recipe1.setAllergies(new ArrayList<>()));
    }

    // Allergies should be able to set to zero, as it is not a required field
//    @Test
//    @DisplayName("Testing the setAllergies method with null value")
//    void testSetAllergiesNull() {
//      assertThrows(IllegalArgumentException.class, () -> recipe1.setAllergies(null));
//    }

    @Test
    @DisplayName("Testing the setCookTime method")
    void testSetCookTime() {
      recipe1.setCookTime(45);
      assertEquals(recipe1.getCookTime(), 45);
    }

    @Test
    @DisplayName("Testing the setIngredients method")
    void testSetIngredients() {

      System.out.println(recipe1.getIngredients().toString());

      ArrayList<FoodItem> foodItemArrayList = new ArrayList<>(Arrays.asList(
              new FoodItem("Flour", 1d, "kg", 1),
              new FoodItem("Milk", 1d, "liter", 2),
              new FoodItem("Eggs", 1d, "stk", 3),
              new FoodItem("Butter", 1d, "kg", 4)));

      recipe1.setIngredients(foodItemArrayList);

      System.out.println(recipe1.getIngredients().toString());

      assertEquals(recipe1.getIngredients(), foodItemArrayList);
    }

    @Test
    @DisplayName("Testing the setIngredients method with empty list")
    void testSetIngredientsEmpty() {
      assertThrows(IllegalArgumentException.class, () -> recipe1.setIngredients(new ArrayList<>()));
    }

    @Test
    @DisplayName("Testing the setIngredients method with null value")
    void testSetIngredientsNull() {
      assertThrows(IllegalArgumentException.class, () -> recipe1.setIngredients(null));
    }
  }


}

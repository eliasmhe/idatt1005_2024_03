package edu.ntnu.stud.idatt1005.team3.model;

import java.util.ArrayList;
import java.util.NoSuchElementException;

/**
 * This class represents a register of recipes. It can store,
 * remove and find recipes from the register.
 *
 * @author augustrb
 * @version 0.2
 * @since 0.1
 */
public class RecipeRegister {
  private ArrayList<Recipe> recipes;

  /**
   * Constructor for the RecipeRegister class. Initializes the recipes ArrayList.
   */
  public RecipeRegister() {
    recipes = new ArrayList<>();
  }

  /**
   * Adds a recipe to the register.
   *
   * @param recipe The recipe to be added to the register.
   */
  public void addRecipe(Recipe recipe) {
    if (recipe == null) {
      throw new NullPointerException("Recipe cannot be null");
    }
    if (recipes.contains(recipe)) {
      throw new IllegalArgumentException("Recipe already in register");
    }
    recipes.add(recipe);
  }

  /**
   * A method to get the recipes in the register.
   *
   * @return The recipes in the register.
   */
  public ArrayList<Recipe> getRecipes() {
    return recipes;
  }

  /**
   * A method to remove a recipe from the register.
   *
   * @param recipe The recipe to be removed from the register.
   */
  public void removeRecipe(Recipe recipe) {
    if (recipe == null) {
      throw new NullPointerException("Recipe cannot be null");
    }
    if (!recipes.contains(recipe)) {
      throw new NoSuchElementException("Recipe not found in register");
    }
    recipes.remove(recipe);
  }

  /**
   * A method to remove a recipe from the register by name.
   *
   * @param recipeName The name of the recipe to be removed from the register.
   */
  public void removeRecipeByName(String recipeName) {
    if (recipeName == null || recipeName.isBlank()) {
      throw new IllegalArgumentException("Recipe name cannot be null or blank");
    }
    if (recipes.isEmpty()) {
      throw new IllegalArgumentException("No recipes in register");
    }
    for (Recipe recipe : recipes) {
      if (recipe.getRecipeName().equals(recipeName)) {
        recipes.remove(recipe);
        break;
      }
    }
    throw new NoSuchElementException("Recipe not found in register");
  }

  /**
   * A method to find a recipe by name.
   *
   * @param recipeName The name of the recipe to be found.
   * @return The recipe with the given name, or null if no recipe with that name is found.
   */
  public Recipe findRecipeByName(String recipeName) {
    if (recipes.isEmpty()) {
      throw new IllegalArgumentException("No recipes in register");
    }
    if (recipeName == null || recipeName.isBlank()) {
      throw new IllegalArgumentException("Recipe name cannot be null or blank");
    }
    for (Recipe recipe : recipes) {
      if (recipe.getRecipeName().equals(recipeName)) {
        return recipe;
      }
    }
    throw new NoSuchElementException("Recipe not found in register");
  }

  /**
   * A method to find recipes by ingredient.
   *
   * @param ingredient The ingredient to be found in the recipes.
   * @return An ArrayList of recipes containing the given ingredient.
   */
  public ArrayList<Recipe> findRecipesByIngredient(String ingredient) {
    if (ingredient == null || ingredient.isBlank()) {
      throw new IllegalArgumentException("Ingredient cannot be null or blank");
    }
    if (recipes.isEmpty()) {
      throw new IllegalArgumentException("No recipes in register");
    }
    ArrayList<Recipe> foundRecipes = new ArrayList<>();
    ArrayList<FoodItem> ingredients = new ArrayList<>();
    for (Recipe recipe : recipes) {
      ingredients = recipe.getIngredients();
      for (FoodItem recipeIngredient : ingredients) {
        if (recipeIngredient.getName().equals(ingredient)) {
          foundRecipes.add(recipe);
          break;
        }
      }
    }
    if (foundRecipes.isEmpty()) {
      throw new IllegalArgumentException("No recipes found with ingredient " + ingredient);
    }
    return foundRecipes;
  }

  /**
   * A method to find recipes by maximum cook time.
   *
   * @param maxCookTime The maximum cook time for the recipes.
   * @return An ArrayList of recipes with a cook time less
   * than or equal to the given maximum cook time.
   */
  public ArrayList<Recipe> findRecipesByMaxCookTime(int maxCookTime) {
    if (recipes.isEmpty()) {
      throw new IllegalArgumentException("No recipes in register");
    }
    if (maxCookTime < 0) {
      throw new IllegalArgumentException("Maximum cook time cannot be negative");
    }
    ArrayList<Recipe> foundRecipes = new ArrayList<>();
    for (Recipe recipe : recipes) {
      if (recipe.getCookTime() <= (maxCookTime)) {
        foundRecipes.add(recipe);
      }
    }
    if (foundRecipes.isEmpty()) {
      throw new IllegalArgumentException(
          "No recipes found with cook time less than or equal to " + maxCookTime);
    }
    return foundRecipes;
  }
}

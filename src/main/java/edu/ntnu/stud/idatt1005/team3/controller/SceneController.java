package edu.ntnu.stud.idatt1005.team3.controller;

import edu.ntnu.stud.idatt1005.team3.database.TheCookbookDB;
import edu.ntnu.stud.idatt1005.team3.model.Recipe;
import edu.ntnu.stud.idatt1005.team3.view.pages.HomePage;
import edu.ntnu.stud.idatt1005.team3.view.pages.InventoryPage;
import edu.ntnu.stud.idatt1005.team3.view.pages.RecipeLibraryPage;
import edu.ntnu.stud.idatt1005.team3.view.pages.ShoppingListPage;
import edu.ntnu.stud.idatt1005.team3.view.pages.SingleRecipePage;
import edu.ntnu.stud.idatt1005.team3.view.pages.subpages.NewRecipePage;
import java.io.FileNotFoundException;
import java.util.Objects;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * The controller for the scenes of the application.
 */
public class SceneController {
  private final Scene scene;
  private final HomePage homePage;
  private final InventoryPage inventoryPage;
  private final RecipeLibraryPage recipePage;
  private final ShoppingListPage shoppingListPage;
  private final TheCookbookDB db;
  private final ShoppingListController slController;

  /**
   * Creates a new scene controller.
   *
   * @param primaryStage The primary stage of the application.
   */
  public SceneController(Stage primaryStage, TheCookbookDB db, ShoppingListController slController)
      throws FileNotFoundException {
    this.scene = new Scene(new StackPane(), 1280, 720);

    scene.getStylesheets().add(
        Objects.requireNonNull(getClass().getResource("/view/stylesheet.css")).toExternalForm()
    );

    primaryStage.setScene(scene);
    this.homePage = new HomePage(this);
    this.inventoryPage = new InventoryPage(this, db);
    this.recipePage = new RecipeLibraryPage(this, db);
    this.shoppingListPage = new ShoppingListPage(this, db, slController);

    this.db = db;
    this.slController = slController;
  }

  /**
   * Shows the home page of the application.
   */
  public void showHomePage() {
    scene.setRoot(homePage.getRoot());
  }

  /**
   * Shows the inventory page of the application.
   */
  public void showInventoryPage() {
    scene.setRoot(inventoryPage.getRoot());
  }

  /**
   * Shows the recipe library page of the application.
   */
  public void showRecipeLibraryPage() {
    scene.setRoot(recipePage.getRoot());
  }

  /**
   * Shows the shopping list page of the application.
   */
  public void showShoppingListPage() {
    scene.setRoot(shoppingListPage.getRoot());
  }

  /**
   * Shows the new recipe page of the application.
   */
  public void showNewRecipePage() throws FileNotFoundException {
    NewRecipePage newRecipePage = new NewRecipePage(this, db);
    scene.setRoot(newRecipePage.getRoot());
  }

  public void showSingleRecipePage(Recipe recipe) throws FileNotFoundException {
    final SingleRecipePage singleRecipePage = new SingleRecipePage(this, db, recipe, slController);
    scene.setRoot(singleRecipePage.getRoot());
  }

  /**
   * Shows an error pop-up with the given error message.
   *
   * @param errorMessage The error message to show in the pop-up.
   */
  public void showErrorPopUp(String errorMessage) {
    Alert a = new Alert(AlertType.NONE);
    // set alert type
    a.setAlertType(AlertType.ERROR);
    a.setContentText(errorMessage);

    // show the dialog
    a.show();


  }
}
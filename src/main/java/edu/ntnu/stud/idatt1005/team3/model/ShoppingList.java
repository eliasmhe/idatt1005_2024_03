package edu.ntnu.stud.idatt1005.team3.model;

import java.util.ArrayList;

/**
 * A class for the CookBook's shopping list. Works with
 * FoodItem class to create a list of food items.
 */
public class ShoppingList {
  private String name;
  private Double quantity;
  private String unit;

  /**
   * Constructs a new shopping list.
   *
   * @param name     The name of the food item.
   * @param quantity The quantity of the food item.
   * @param unit     The unit of the food item.
   */
  public ShoppingList(String name, Double quantity, String unit) {
    this.name = name;
    this.quantity = quantity;
    this.unit = unit;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Double getQuantity() {
    return quantity;
  }

  public void setQuantity(Double quantity) {
    this.quantity = quantity;
  }

  public String getUnit() {
    return unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }

  /**
   * A list of food items.
   */
  private ArrayList<FoodItem> foodItems;

  /**
   * Constructs a new shopping list.
   *
   * @param foodItems A list of food items.
   */

  public ShoppingList(ArrayList<FoodItem> foodItems) {
    setFoodItems(foodItems);
  }

  private void setFoodItems(final ArrayList<FoodItem> foodItems) {
    if (foodItems == null) {
      throw new NullPointerException("Food items cannot be null.");
    }
    this.foodItems = foodItems;
  }

  /**
   * Adds a food item to the shopping list.
   *
   * @param foodItem The food item to add.
   */
  public void addFoodItem(FoodItem foodItem) throws NullPointerException {
    if (foodItem == null) {
      throw new NullPointerException("Food item cannot be null.");
    }
    foodItems.add(foodItem);
    System.out.println(foodItem.getName() + " has been added to the shopping list.");
  }

  /**
   * Removes a food item from the shopping list.
   *
   * @param foodItem The food item to remove.
   */
  public void removeFoodItem(FoodItem foodItem) throws NullPointerException {
    if (foodItem == null) {
      throw new NullPointerException("Food item cannot be null.");
    }
    foodItems.remove(foodItem);
    System.out.println(foodItem.getName() + " has been removed from the shopping list.");
  }

  /**
   * Finds a food item in the shopping list by its name.
   *
   * @param name The name of the food item to find.
   * @return The food item if it is found, null if it is not found.
   */
  public FoodItem findFoodItemByName(String name) {
    if (name == null) {
      throw new NullPointerException("Name cannot be null.");
    }
    return foodItems.stream()
        .filter(foodItem ->
            foodItem.getName()
                .equalsIgnoreCase(name))
        .findFirst()
        .orElse(null);
  }

  /**
   * Finds a food item in the shopping list by its ID.
   * If the ID is negative, an IllegalArgumentException is thrown.
   *
   * @param id The ID of the food item to find.
   * @return The food item if it is found, null if it is not found.
   */
  public FoodItem findFoodItemById(int id) {
    if (id < 0) {
      throw new IllegalArgumentException("ID cannot be negative.");
    }
    return foodItems.stream()
        .filter(foodItem ->
            foodItem.getId() == id)
        .findFirst()
        .orElse(null);
  }

  /**
   * Prints the shopping list.
   */
  public void printShoppingList() {
    for (FoodItem foodItem : foodItems) {
      System.out.println(foodItem.getName() + "\t- " + foodItem.getQuantity());
    }
  }

  /**
   * Returns the list of food items.
   *
   * @param recipe The recipe to add to the shopping list.
   */
  public void addRecipeToShoppingList(Recipe recipe) {
    for (FoodItem ingredient : recipe.getIngredients()) {
      FoodItem foodItem = findFoodItemByName(ingredient.getName());
      if (foodItem != null) {
        foodItems.add(foodItem);
      }
    }
  }

  /**
   * Returns the list of food items.
   *
   * @return The list of food items.
   */

  public ArrayList<FoodItem> getFoodItems() {
    return foodItems;
  }
}

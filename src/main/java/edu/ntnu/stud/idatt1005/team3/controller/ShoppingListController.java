package edu.ntnu.stud.idatt1005.team3.controller;

import edu.ntnu.stud.idatt1005.team3.database.TheCookbookDB;
import edu.ntnu.stud.idatt1005.team3.model.FoodItem;
import java.util.ArrayList;
import java.util.List;

/**
 * A class that generates a shopping list and alters it.
 */
public class ShoppingListController {
  private List<FoodItem> shoppingList;
  TheCookbookDB db;

  public ShoppingListController(TheCookbookDB db) {
    this.shoppingList = new ArrayList<>();
    this.db = db;
  }

  /**
   * Adds ingredients from a chosen recipe to the shopping list.
   *
   * @param recipeId the id of the recipe you want to use.
   */
  public void addRecipeIngredients(TheCookbookDB db, int recipeId) {
    List<FoodItem> ingredients = db.getIngredientsForRecipe(recipeId);
    for (FoodItem ingredient : ingredients) {
      addOrUpdateIngredient(ingredient.getName(), ingredient.getQuantity(), ingredient.getUnit());
    }
  }

  /**
   * Adds an individual ingredient.
   *
   * @param ingredientName the ingredient you want to add.
   */
  public void addIngredient(String ingredientName, Double quantity, String unit) {
    addOrUpdateIngredient(ingredientName, quantity, unit);
  }

  /**
   * Checks if an ingredient exists in the list,
   * so it can update it instead of adding it multiple times.
   *
   * @param ingredientName the name of the ingredient.
   * @param quantity       the quantity of the chosen ingredient.
   * @param unit           the unit of measurement.
   */
  private void addOrUpdateIngredient(String ingredientName, Double quantity, String unit) {
    boolean found = false;
    for (FoodItem item : shoppingList) {
      if (item.getName().equals(ingredientName)) {
        item.setQuantity(item.getQuantity() + quantity);
        found = true;
        break;
      }
    }
    if (!found) {
      shoppingList.add(new FoodItem(ingredientName, quantity, unit, 0));
    }
  }

  /**
   * Generates the final shopping list.
   *
   * @return Returns the shopping list.
   */
  public List<FoodItem> generateShoppingList() {
    return shoppingList;
  }

  public void clear() {
    shoppingList.clear();
  }

  /**
   * Checks the shopping list against the inventory in the database.
   * Updates the inventory by removing if we have it, or adding to a final list what we are missing.
   *
   * @param db the database.
   */
  public List<FoodItem> checkInventory(TheCookbookDB db, List<FoodItem> shoppingList) {
    List<FoodItem> missingItems = new ArrayList<>();
    for (FoodItem item : shoppingList) {
      String itemName = item.getName();
      Double requiredQuantity = item.getQuantity();

      // Get the quantity of the item in the database
      FoodItem inventoryItem = db.getFoodItem(itemName);
      if (inventoryItem != null) {
        Double availableQuantity = inventoryItem.getQuantity();
        if (availableQuantity >= requiredQuantity) {
          // Remove the required quantity from the inventory
          db.updateFoodItemQuantity(inventoryItem.getId(), availableQuantity - requiredQuantity);
        } else {
          // Not enough in inventory, set quantity to 0 and add to missing items list
          db.updateFoodItemQuantity(inventoryItem.getId(), 0d);
          double missingQuantity = requiredQuantity - availableQuantity;
          missingItems.add(
              new FoodItem(itemName, missingQuantity, item.getUnit(), inventoryItem.getId()));
        }
      } else {
        // Item doesn't exist in inventory, add to missing items list
        db.insertFoodItem(itemName, 0, item.getUnit()); // Insert the item into the database
        missingItems.add(new FoodItem(itemName, requiredQuantity, item.getUnit(), -1));
      }
    }

    // Clear the current shopping list and add missing items to it
    shoppingList.clear();
    for (FoodItem missingItem : missingItems) {
      shoppingList.add(
          new FoodItem(missingItem.getName(), missingItem.getQuantity(), missingItem.getUnit(), 0));
    }
    System.out.println(missingItems.toString());
    return missingItems;
  }
}
